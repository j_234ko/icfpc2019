#!/bin/sh
for p in ${PROBLEM_DIR:-/prob}/*.desc
do
  s="$(basename ${p} .desc).sol"
  ./${TARGET} ${p} --no-debug > ${SOLVE_DIR:-/sol}/${s}
done
