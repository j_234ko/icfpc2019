#!/bin/bash

function check_pid {
    pid=$1
    if [[ -n ${pid} && ${pid} -gt 0 && $( ps -o pid -p ${pid} | wc -l ) -eq 2 ]]; then
        return 0
    else
        return 1
    fi
}

function stop_celeryd {
    pid=$(cat $HOME/celery.pid)
    if check_pid ${pid}; then
        kill -TERM ${pid} 2>/dev/null
        elapsed=0
        while check_pid ${pid}; do
            if [[ ${elapsed} -gt 30 ]]; then
                kill -TERM ${pid} 2>/dev/null
            fi
            sleep 1
            (( elapsed += 1 ))
        done
    fi
}

COMMAND=$1

case ${COMMAND} in
    "start")
        if [ -e $HOME/celery.pid ]; then
            stop_celeryd
        fi
        $HOME/.pyenv/versions/exec/bin/celery worker --detach \
                                              -A tasks \
                                              --loglevel=info \
                                              --pidfile=$HOME/celery.pid \
                                              --logfile=$HOME/celery.log
        ;;
    "stop")
        if [ -e $HOME/celery.pid ]; then
            stop_celeryd
        fi
        ;;
    "purge")
        $HOME/.pyenv/versions/exec/bin/celery purge -A tasks
        ;;
    *)
        exit 0
esac
