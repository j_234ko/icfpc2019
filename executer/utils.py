import boto3
from pathlib import Path


s3 = boto3.resource('s3')
bucket = s3.Bucket('cirimenjao')

def upload(uniq_key: str, path: Path):
    bucket.upload_file(Filename=str(path),
                       Key=f'{uniq_key}/{path.name}')


def download(uniq_key: str, path: Path):
    bucket.download_file(Key=f'{uniq_key}/{path.name}',
                         Filename=str(path))


def make_dirs(path: Path):
    try:
        path.mkdir()
    except FileNotFoundError:
        make_dirs(path.parent)
        path.mkdir()
    except FileExistsError:
        pass
