import subprocess
import utils
from zipfile import ZipFile
from pathlib import Path
from celery import Celery


app = Celery('wrappy_executer')
app.config_from_object('celeryconfig')

root = Path.home() / 'icfpc2019'
sol_dir = root / 'sol'


class CompileError(Exception):
    pass


class RetryError(Exception):
    pass


def compile_(uniq_key: str, target: str, target_dir: Path) -> Path:
    target_path = target_dir / target
    utils.download(uniq_key, target_path)

    with ZipFile(target_path) as f:
        f.extractall(path=target_dir)

    compiler_path = target_dir / 'compile.sh'
    compiler_path.chmod(0o755)
    exe_path = target_dir / 'a.out'
    try:
        proc = subprocess.Popen([str(compiler_path), str(exe_path)],
                                cwd=compiler_path.parent,
                                stderr=subprocess.PIPE,
                                stdout=subprocess.PIPE)
    except OSError:
        raise RetryError()

    try:
        _, stderr = proc.communicate(timeout=30)
    except subprocess.TimeoutExpired:
        proc.kill()
        raise CompileError('コンパイルがタイムアウトしました')

    if not exe_path.exists():
        raise CompileError('実行ファイルが出力されていません')


@app.task(bind=True)
def execute(self, uniq_key: str, prob_uniq_key: str, target: str, prob: str):
    """
    target で指定されたパスの zip ファイルを解凍してプログラムを実行、
    指定された問題に対する結果ファイルを出力する

    Args:
        uniq_key(str): S3バケットのユニークキー
        prob_uniq_key(str): 問題のユニークキー
        target_zip_path(str): zipファイルのファイル名
        prob(str): 問題の文字列
    """
    target_dir = Path.home() / 'work' / uniq_key
    utils.make_dirs(target_dir)
    exe_path = target_dir / 'a.out'

    if not exe_path.exists():
        try:
            compile_(uniq_key, target, target_dir)
        except RetryError:
            self.retry(countdown=1)

    prob_path = target_dir / f'{prob_uniq_key}.desc'
    with open(str(prob_path), 'w') as f:
        f.write(prob)

    sol_path = target_dir / prob_path.name.replace('.desc', '.sol')
    sol_file = open(str(sol_path), 'w')

    try:
        proc = subprocess.Popen([str(exe_path), str(prob_path)],
                                cwd=exe_path.parent,
                                stdout=sol_file)
    except OSError:
        print(f'retry on run: {prob_path}')
        self.retry(countdown=1)

    try:
        _, stderr = proc.communicate(timeout=3600)
    except subprocess.TimeoutExpired:
        proc.kill()

    utils.upload(uniq_key, sol_path)
