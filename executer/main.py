import sys
import utils
from datetime import datetime
from pathlib import Path
from tasks import execute

root = Path.home() / 'icfpc2019'
prob_dir = root / 'tasks'
target_path = Path(sys.argv[1])
date_str = datetime.now().strftime('%m%d%H%M')

for prob_id in range(300, 0, -1):
    prob_name = f'prob-{str(prob_id).zfill(3)}.desc'
    prob_path = prob_dir / prob_name

    with open(str(prob_path)) as f:
        prob = f.read()

    utils.upload(date_str, target_path)
    execute.delay(date_str, prob_path.name.replace('.desc', ''),
                  target_path.name, prob)
