from pathlib import Path
from flask import Flask, render_template, jsonify, request
from tasks import execute

app = Flask('cirimenjao-executer')


@app.route('/execute/<uniq_key>/<prob_id>/', methods=['POST'])
def execute_task(uniq_key, prob_id):
    prob_dir = Path.home() / 'icfpc2019' / 'tasks'
    prob_name = f'prob-{str(prob_id).zfill(3)}.desc'
    prob_path = prob_dir / prob_name

    with open(str(prob_path)) as f:
        prob = f.read()

    execute.delay(uniq_key, prob_name.replace('.desc', ''),
                  'ai.zip', prob)

    return jsonify({'result': 'success'})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=2222)
