import os
import requests
from html.parser import HTMLParser


def create_table(keys, values):
    column_length = {}
    for k in keys:
        max_length = len(k)
        for v in values:
            max_length = max(max_length, len(str(v[k])))
        column_length[k] = max_length

    result = ''
    for k in keys:
        result += '| %s ' % k + ' ' * (
            column_length[k] - len(k))
    result += '|\n'
    for k in keys:
        result += '|:' + '-' * (column_length[k] + 1)
    result += '|\n'
    for v in values:
        for k in keys:
            result += '| %s ' % v[k] + ' ' * (
                column_length[k] - len(str(v[k])))
        result += '|\n'

    return result


class RankParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self.th = False
        self.td = False
        self.data = []

    def handle_starttag(self, tag, attrs):
        if tag == 'th':
            self.data.append({})
            self.th = True
        if tag == 'td':
            self.td = True

    def handle_data(self, data):
        if self.th:
            self.data[-1] = {'key': data}
            self.th = False
        if self.td:
            if 'value' in self.data[-1]:
                self.data[-1]['value'].append(data)
            else:
                self.data[-1].update({'value': [data]})
            self.td = False


r = requests.get('https://monadic-lab.org/rankings/latest.html')
parser = RankParser()
parser.feed(r.text)
parser.close()

msg = ''
results = []
for d in parser.data:
    if 'value' not in d:
        continue
    if (d['key'] in ('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')
            or 'cirimenjao' in d['value'] or 'Gon the Fox' in d['value']):
        results.append({
            'rank': d['key'],
            'name': d['value'][0],
            'score': d['value'][1],
            'score + unspent LAM': d['value'][2],
        })

msg = f"""ランキングだよ
```
{create_table(['rank', 'name', 'score', 'score + unspent LAM'], results)}```
"""

slack_url = os.environ['SLACK_URL']
requests.post(slack_url, json={
    'text': msg, 'channel': 'icfp-pc2019',
    'icon_emoji': ':icfp-pc:', 'username': 'ranking_bot'})
