#!/bin/bash
block=$1
puzzle_exe_path="../puzzle/a.out"
task_exe_path="../ai/a.out"
puzzle_path="../puzzle-task/puzzle_$1.cond"
puzzle_sol_path="../puzzle-sol/puzzle_sol_$1.desc"
task_sol_path="../task-sol/task_sol_$1.sol"

./lambda-cli.py getblockinfo $1 puzzle > $puzzle_path
../puzzle/a.out $puzzle_path $puzzle_sol_path
../ai/a.out $puzzle_sol_path > $task_sol_path
./lambda-cli.py submit $1 $task_sol_path $puzzle_sol_path
