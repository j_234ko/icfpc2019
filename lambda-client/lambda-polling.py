#!/usr/bin/env python3
import os
import time
import subprocess
import requests
from pathlib import Path
from jsonrpc_requests import Server

server = Server(f'http://127.0.0.1:8332')


class RetryError(Exception):
    pass


def get_puzzle(block=None):
    if block:
        bi = server.getblockinfo(block)
    else:
        bi = server.getblockinfo()
    return bi['block'], bi['puzzle']


def get_balance():
    return server.getbalance()


def submit(block, task_sol_path, puzzle_sol_path):
    resp = server.submit(block, task_sol_path, puzzle_sol_path)
    return resp


def poll():
    while True:
        try:
            target_block, puzzle = get_puzzle()
            puzzle_path = f'../puzzle-task/puzzle_{target_block}.cond'
            if not Path(puzzle_path).exists():
                break
            else:
                raise RetryError('submitted')
        except KeyboardInterrupt:
            return
        except RetryError:
            time.sleep(1)
        except Exception as e:
            print(e)
            time.sleep(1)

    print(target_block)
    with open(puzzle_path, 'w') as f:
        f.write(puzzle)

    puzzle_exe_path = '../puzzle/a.out'
    puzzle_sol_path = f'../puzzle-sol/puzzle_sol_{target_block}.desc'
    print(' '.join(
        [puzzle_exe_path, puzzle_path, puzzle_sol_path, '--no-debug']))
    proc = subprocess.run([puzzle_exe_path, puzzle_path, puzzle_sol_path,
                           '--no-debug'])

    task_exe_path = '../ai/a.out'
    task_sol_path = f'../task-sol/task_sol_{target_block}.sol'
    print(' '.join([task_exe_path, puzzle_sol_path, '--no-debug']))
    proc = subprocess.run([task_exe_path, puzzle_sol_path, '--no-debug'],
                          stdout=open(task_sol_path, 'w'))
    print('task solved.')

    resp = submit(target_block, task_sol_path, puzzle_sol_path)
    print(resp)
    return resp


def notice(result):
    balance = get_balance()
    msg = f'提出したよ ```{result}```\n現在のbalance: {balance}'
    slack_url = os.environ['SLACK_URL']
    requests.post(slack_url, json={
        'text': msg, 'channel': 'icfp-pc2019',
        'icon_emoji': ':icfp-pc:', 'username': 'puzzle_bot'})

def main():
    while True:
        try:
            result = poll()
            notice(result)
        except KeyboardInterrupt:
            return


if __name__ == '__main__':
    main()
