FROM python:alpine3.9
ARG problem="tasks/part-1-initial"
RUN apk --update add g++\
    make\
    cmake\
    ca-certificates
ARG target_dir
ENV PROBLEM_DIR=/prob
ENV SOLVE_DIR=/sol
ENV TARGET icfpc2019
COPY ${target_dir} /repos
WORKDIR /build
RUN cmake /repos && make
COPY entrypoint.sh .
ENTRYPOINT ["./entrypoint.sh"]
