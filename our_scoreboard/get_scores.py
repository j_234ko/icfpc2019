import requests
from bs4 import BeautifulSoup
from collections import OrderedDict

root_url = "https://monadic-lab.org/grades/4e6e9a87b9e0fab6accf7f59/"
score_filename = "score.csv"
max_prog = 300

def run() -> None:
    root_page = requests.get(root_url)
    soup = BeautifulSoup(root_page.content, "html.parser")
    
    result = OrderedDict()
    update_date = dict()
    result_cnt = 0
    for link in soup.find_all("a"):
        link_name = str(link.text)
        if not link_name.startswith('2019'):
            # テキストファイルとか
            continue
        print(link_name)
        csv_url = root_url + link_name + score_filename
        csv_content = requests.get(csv_url).text
        data = OrderedDict()
        for row in csv_content.split("\n"):
            try:
                prog_no, score, status = [s.strip() for s in row.split(",")]
                data[int(prog_no)] = {
                    "score": int(score),
                    "status": status.lower(),
                }
            except:
                pass
        len_data = len(data)
        # 足りない文を埋める
        for i in range(len_data + 1, max_prog + 1):
            data[i] = {
                "score": 0,
                "status": "None",
            }
        result[result_cnt] = data
        update_date[result_cnt] = link_name[:-8]
        result_cnt += 1

    table = [[None for _ in range(max_prog)] for _ in range(result_cnt)]
    max_num = [666655301 for _ in range(max_prog)]
    for k, v in result.items():
        for k2, v2 in v.items():
            score = v2['score']
            table[k][k2 - 1] = score
            if score > 0:
                max_num[k2 - 1] = min(max_num[k2 - 1], score)

    body = "| times | " + " | ".join([str(i) for i in range(1, max_prog + 1)]) + " |\n"
    body += "| -- " * 301 + " |\n"
    for i, t in enumerate(table):
        tt = t
        for i2, elm in enumerate(t):
            if elm == 0:
                tt[i2] = f"[- {elm} -]"
            elif elm == max_num[i2]:
                tt[i2] = f"[+ {elm} +]"
            else:
                tt[i2] = f"{elm}"
        body += f"| {update_date[i]} | " + " | ".join([j for j in tt]) + " |\n"
    f = open('our_scoreboard.md','w')
    f.write(body)
    f.close()

if __name__ == "__main__":
    run()