# 使い方

* `pipenv install` する（いつもどおり）
* `pipenv run update` する
* `git add our_scoreboard.md` をしてあげる

# 注意点

色付けは頑張りましたが、 gitlab が残念な感じなのでこれで許してください。

* 赤色: 0点の提出
* 緑色: ベストスコア（0点以上で、かつ最小値）
* その他: 普通の点数