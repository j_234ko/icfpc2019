#include "lib.cpp"


class StateWrapper {

    Trace trace;
    State state;
    State init_state;

public:

    explicit StateWrapper(State init_state, State state, Trace trace)
            : init_state(std::move(init_state)),
              state(std::move(state)),
              trace(std::move(trace)) {}

    State get_state() {
        return state;
    }

    Trace get_trace() {
        return trace;
    }


};

static int turn_type = 0;

Trace bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (!sys.is_obstacle(next_state.robot_list[idx].pos) &&
                !sys.is_wrapped(next_state.robot_list[idx].pos)) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}


Trace manipulator_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int manipulator_count = init_state.robot_list[idx].count_booster_manipulator();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_manipulator() > manipulator_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}

Trace cloning_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int cloning_count = init_state.robot_list[idx].count_booster_cloning();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_cloning() > cloning_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}


Trace x_fields_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (sys.is_x_field(next_state.robot_list[idx].pos)) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}


Trace teleport_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        if (trace.get_command_list().size() >= 10) {
            return Trace();
        }

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (sys.get_current_state().robot_list[idx].have_booster_teleport()) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();
}

Trace fast_wheels_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        if (trace.get_command_list().size() >= 10) {
            return Trace();
        }

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (sys.get_current_state().robot_list[idx].have_booster_fast_wheels()) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();
}

Trace drill_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        if (trace.get_command_list().size() >= 10) {
            return Trace();
        }

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (sys.get_current_state().robot_list[idx].have_booster_drill()) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();
}

bool step(System &system) {

    static int count = 0;

    for (int idx = 0; idx < system.get_current_state().robot_list.size(); idx++) {

        //dump(count);
        count++;

//        system.debug_wrapped();
//        system.get_current_state().debug();

        // 全てラップしていれば終了
        if (system.is_all_wrapped()) {
            return true;
        }

        //最初に回転する
        if (turn_type > 0) {
            turn_type--;
            Command cmd = Command::create_turn_manipulators_command(idx, true);
            system.exec_command(cmd);
            continue;
        } else if (turn_type < 0) {
            turn_type++;
            Command cmd = Command::create_turn_manipulators_command(idx, false);
            system.exec_command(cmd);
            continue;
        }

        // x フィールドにおらず テレポートを所持していれば使う
        if (!system.is_x_field(system.get_current_state().robot_list[idx].pos) &&
            system.get_current_state().robot_list[idx].have_booster_teleport()) {
            system.teleport_reset(idx);
            continue;
        }

        // ドリルを所持していれば使う
        if (system.get_current_state().robot_list[idx].have_booster_drill()) {
            system.drill(idx);
            continue;
        }

        // ファストホイールを所持していれば使う
        if (system.get_current_state().robot_list[idx].have_booster_fast_wheels()) {
            system.fast_wheels(idx);
            continue;
        }

        // マニピュレータを所持していれば使う
        if (system.get_current_state().robot_list[idx].have_booster_manipulator()) {
            system.extension_of_the_manipulator(idx);
            continue;
        }

        // x フィールドにいて cloning があれば clone
        if (system.is_x_field(system.get_current_state().robot_list[idx].pos) &&
            system.get_current_state().robot_list[idx].have_booster_cloning()) {
            system.cloning(idx);
            continue;
        }

        // cloning があれば X を探す
        if (system.get_current_state().robot_list[idx].have_booster_cloning()) {
            Trace trace = x_fields_bfs(idx, system, system.get_current_state());

            //assert(trace.empty());

            for (const auto &command: trace.get_command_list()) {
                system.exec_command(command);
            }
            continue;
        }

        if (system.get_current_state().exists_cloning()) {
            // cloning を探す
            Trace trace = cloning_bfs(idx, system, system.get_current_state());

            //assert(trace.empty());

            for (const auto &command: trace.get_command_list()) {
                system.exec_command(command);
            }
            continue;
        }

        if (system.get_current_state().exists_drill()) {
            // drill を探す
            Trace trace = drill_bfs(idx, system, system.get_current_state());

            if (!trace.empty()) {
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                continue;
            }
        }

        if (system.get_current_state().exists_fast_wheels()) {
            // fast_wheels を探す
            Trace trace = fast_wheels_bfs(idx, system, system.get_current_state());

            if (!trace.empty()) {
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                continue;
            }
        }

        if (system.get_current_state().exists_cloning()) {
            // cloning を探す
            Trace trace = cloning_bfs(idx, system, system.get_current_state());

            if (!trace.empty()) {
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                continue;
            }
        }

        if (system.get_current_state().exists_teleport()) {

            // teleport を探す
            Trace trace = teleport_bfs(idx, system, system.get_current_state());

            if (!trace.empty()) {
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                continue;
            }
        }

        if (system.get_current_state().exists_manipulator()) {

            // マニピュレータを探す
            Trace trace = manipulator_bfs(idx, system, system.get_current_state());

            if (!trace.empty()) {
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                continue;
            }
        }

        Trace trace = bfs(idx, system, system.get_current_state());

        if (trace.empty()) {
            //assert(system.is_all_wrapped());
            system.error_end = true;
            return true;
        }

        for (const auto &command: trace.get_command_list()) {
            system.exec_command(command);
        }
    }

    return false;
}

int main(int argc, char *argv[]) {

    System system = init_system(argc, argv);
    vector<pair<int, string>> ans;
    for (int i = 0; i < 8; i++) {
        system = init_system(argc, argv);
        reverse_dir = (i/4 == 1);
        turn_type = (i%4)-1;
        while (true) {

            if (step(system)) break;

        }

        if (!system.error_end) {

            auto calc_score = system.calc_score();
            ans.push_back(calc_score);
            //cout << calc_score.first << endl;
            //system.output_solution();
        }
    }
    sort(ans.begin(), ans.end());
    if (ans.size() > 0) {
        //cout << ans[0].first << endl;
        cout << ans[0].second << endl;
    }

    return 0;
}