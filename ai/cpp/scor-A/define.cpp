#ifndef DEFINE_CPP
#define DEFINE_CPP

#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <functional>
#include <queue>
#include <stack>
#include <cmath>
#include <iomanip>
#include <list>
#include <tuple>
#include <bitset>
#include <ciso646>
#include <cassert>
#include <complex>
#include <random>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <chrono>
#include <ratio>
#include <bitset>
#include <fstream>

#include <cstdio>
#include <ctime>
#include <cstdio>
#include <sys/types.h>
#include <sys/timeb.h>

#define rep(i, n)    for(int (i) = 0; (i) < (n); (i)++)
#define REP(i, a, n)    for(int (i) = (a); (i) < (n); (i)++)
#define each(i, n)    for(auto &i : n)
#define clr(a)        memset((a), 0 ,sizeof(a))
#define mclr(a)        memset((a), -1 ,sizeof(a))
#define all(a)        (a).begin(),(a).end()
#define dum(val)     cerr << #val " = " << val << " ";
#define dump(val)    cerr << #val " = " << val << endl;
#define abs(a)       ((a) > 0 ? (a) : -(a))

using namespace std;

template<class T>
string tostr(T x) {
    stringstream o;
    o << x;
    return o.str();
}


int toint(const string &s) {
    stringstream ss;
    ss << s;
    int v;
    ss >> v;
    return v;
}

int reverse_move_dir(int move_dir) {
    return (move_dir + 2) % 4;
}

bool check(int x, int y, int W, int H) {
    return x >= 0 && y >= 0 && x < W && y < H;
}

const int dx[8] = {+1, 0, -1, 0, +1, -1, -1, +1};
const int dy[8] = {0, -1, 0, +1, +1, -1, +1, -1};

const int RIGHT = 0;
const int DOWN = 1;
const int LEFT = 2;
const int UP = 3;
const int RIGHT_UP = 4;
const int LEFT_DOWN = 5;
const int LEFT_UP = 6;
const int RIGHT_DOWN = 7;

const int FAST_WHEELS_ACTIVATE_TURN_MAX = 50;
const int DRILL_ACTIVATE_TURN_MAX = 30;

const char MANIPULATOR_CODE = 'B';
const char FAST_WHEELS_CODE = 'F';
const char DRILL_CODE = 'L';
const char TELEPORT_CODE = 'R';
const char CLONING_CODE = 'C';
const char X_CODE = 'X';


struct Point {
    int x, y;

    Point() { x = -1, y = -1; }

    Point(int x, int y)
            : x(x), y(y) {}

    int dist(const Point &other) {
        return abs(x - other.x) + abs(y - other.y);
    }

    bool operator<(const Point &other) const {
        return x < other.x || (x == other.x && y < other.y);
    }

    bool operator==(const Point &other) const {
        return (x == other.x && y == other.y);
    }
};

vector<Point> WRAP_RANGE_LIST[4];

vector<Point> get_wrap_range_list(int dir, int manipulator_size) {
    return vector<Point>(WRAP_RANGE_LIST[dir].begin(), WRAP_RANGE_LIST[dir].begin() + manipulator_size);
}

void init_define() {
    WRAP_RANGE_LIST[RIGHT].emplace_back(0, 0);
    WRAP_RANGE_LIST[RIGHT].emplace_back(+1, +1);
    WRAP_RANGE_LIST[RIGHT].emplace_back(+1, 0);
    WRAP_RANGE_LIST[RIGHT].emplace_back(+1, -1);

    for (int i = 1; i <= 100; i++) {
        WRAP_RANGE_LIST[RIGHT].emplace_back(0, +i);
        WRAP_RANGE_LIST[RIGHT].emplace_back(0, -i);
    }

    WRAP_RANGE_LIST[UP].emplace_back(0, 0);
    WRAP_RANGE_LIST[UP].emplace_back(-1, +1);
    WRAP_RANGE_LIST[UP].emplace_back(0, +1);
    WRAP_RANGE_LIST[UP].emplace_back(+1, +1);

    for (int i = 1; i <= 100; i++) {
        WRAP_RANGE_LIST[UP].emplace_back(+i, 0);
        WRAP_RANGE_LIST[UP].emplace_back(-i, 0);
    }

    WRAP_RANGE_LIST[DOWN].emplace_back(0, 0);
    WRAP_RANGE_LIST[DOWN].emplace_back(-1, -1);
    WRAP_RANGE_LIST[DOWN].emplace_back(0, -1);
    WRAP_RANGE_LIST[DOWN].emplace_back(1, -1);

    for (int i = 1; i <= 100; i++) {
        WRAP_RANGE_LIST[DOWN].emplace_back(+i, 0);
        WRAP_RANGE_LIST[DOWN].emplace_back(-i, 0);
    }

    WRAP_RANGE_LIST[LEFT].emplace_back(0, 0);
    WRAP_RANGE_LIST[LEFT].emplace_back(-1, +1);
    WRAP_RANGE_LIST[LEFT].emplace_back(-1, 0);
    WRAP_RANGE_LIST[LEFT].emplace_back(-1, -1);

    for (int i = 1; i <= 100; i++) {
        WRAP_RANGE_LIST[LEFT].emplace_back(0, +i);
        WRAP_RANGE_LIST[LEFT].emplace_back(0, -i);
    }
}

const int INF = 1e9;


#endif
