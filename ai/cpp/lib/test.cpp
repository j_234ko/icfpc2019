//
// Created by o on 19/06/21.
//

#include "lib.cpp"

int main(int argc, char *argv[]) {

    System system = init_system(argc, argv);

    system.debug();

    system.output_solution();

    return 0;
}