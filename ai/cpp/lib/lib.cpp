#include <utility>

#ifndef LIB_CPP
#define LIB_CPP


#include "define.cpp"
#include "command.cpp"


vector<string> split(string s, string t) {
    vector<string> ret;
    ret.emplace_back();

    int ts = t.size();
    rep(i, s.size()) {
        if (s.substr(i, ts) == t) {
            i += ts - 1;
            ret.emplace_back();
        } else {
            ret[ret.size() - 1] += s[i];
        }
    }

    if (ret[ret.size() - 1].empty()) ret.pop_back();
    return ret;
}

void dump_vec2(const vector<vector<int>> &v2) {

    int w = v2.size();
    int h = v2[0].size();

    dum(w)
    dump(h)

    for (int y = v2[0].size() - 1; y >= 0; y--) {
        for (int x = 0; x < v2.size(); x++) {
            cerr << v2[x][y];
        }
        cerr << endl;
    }
    cerr << endl;

}


class Trace {
    vector<Command> command_list;

public:


    void append(const Command &command) {
        command_list.push_back(command);
    }

    bool empty() {
        return command_list.empty();
    }

    vector<Command> get_command_list() {
        return command_list;
    }

    void output() {
        for (auto command: command_list) {
            cout << command.to_string();
        }
        cout << endl;
    }


};

class StaticData {
public:
    static vector<vector<int>> fields;
};

/**
 * 障害物が無い: 0
 * 障害物がある: 1
 */
vector<vector<int>> StaticData::fields;

class State {

private:

public:

    int W, H;

    int dir;

    Point pos;

    vector<Point> wrap_range;

    State(int W, int H, int dir, vector<Point> wrap_range, Point pos)
            : W(W),
              H(H),
              dir(dir),
              wrap_range(std::move(wrap_range)),
              pos(pos) {}

    void debug() {
        dum(pos.x)
        dump(pos.y)
    }

    /**
     * move_dir の方向にロボットが移動可能か判定する
     */
    bool can_move(int move_dir) {

        int tx = pos.x + dx[move_dir];
        int ty = pos.y + dy[move_dir];

        return tx >= 0 && ty >= 0 && tx < W && ty < H && StaticData::fields[tx][ty] == 0;
    }


    /**
     * move_dir の方向にロボットを移動させる
     */
    Command move(int move_dir) {

        int tx = pos.x + dx[move_dir];
        int ty = pos.y + dy[move_dir];

        assert (tx >= 0 && ty >= 0 && tx < W && ty < H);
        assert (StaticData::fields[tx][ty] == 0);

        pos.x = tx;
        pos.y = ty;

        return Command::create_move_command(move_dir);
    }

    /**
     * マニピュレータを回転させる
     */
    Command turn(bool is_clockwise) {

        if (is_clockwise) {
            dir = (dir + 1) % 4;
        } else {
            dir = (dir + 3) % 4;
        }

        wrap_range = WRAP_RANGE_LIST[dir];

        return Command::create_turn_manipulators_command(is_clockwise);
    }

    /**
     * 移動可能な方向を列挙する
     */
    vector<int> move_dir_list() {

        vector<int> res;

        rep(move_dir, 4) {
            if (can_move(move_dir)) {
                res.push_back(move_dir);
            }
        }

        return res;
    }

    /**
     * 繊維可能な状態を列挙する
     * pair.first = その状態に遷移するためのコマンド
     * pair.second = 遷移後の状態
     */
    vector<pair<Command, State>> next_command_to_state_list() {

        vector<int> dir_list = move_dir_list();

        vector<pair<Command, State>> state_list;

        for (const auto &_dir : dir_list) {

            State next_state = *this;
            auto command = next_state.move(_dir);
            state_list.emplace_back(command, next_state);

        }

        rep(is_clockwise, 2) {

            State next_state = *this;
            auto command = next_state.turn(is_clockwise);
            state_list.emplace_back(command, next_state);

        }

        return state_list;
    }

};


class System {

private:

    Point init_pos;
    Trace trace;
    State current_state;
    vector<vector<int>> wrapped;
    int wrap_count;
    int wrap_count_max;

    void wrap() {

        for (auto p : current_state.wrap_range) {

            int tx = current_state.pos.x + p.x;
            int ty = current_state.pos.y + p.y;

            if (!check(tx, ty, current_state.W, current_state.H)) {
                continue;
            }

            if (StaticData::fields[tx][ty] == 1) {
                continue;
            }

            if (!wrapped[tx][ty]) {
                wrap_count += 1;
                assert(wrap_count <= wrap_count_max);
            }

            wrapped[tx][ty] = 1;
        }

    }

public:


    System(int W, int H, Point init_pos)
            : init_pos(init_pos),
              current_state(W, H, RIGHT, WRAP_RANGE_LIST[RIGHT], init_pos),
              wrapped(W, vector<int>(H, 0)),
              wrap_count(0),
              wrap_count_max(0) {

        rep(x, W) {
            rep(y, H) {
                if (StaticData::fields[x][y] == 0) {
                    wrap_count_max += 1;
                }
            }
        }

    }

    void debug() {

        int W = current_state.W;
        int H = current_state.H;

        dum(W)
        dump(H)

        dum(init_pos.x)
        dump(init_pos.y)

        dump_vec2(StaticData::fields);

    }

    void debug_wrapped() {

        vector<vector<int>> fixed_wrapped(current_state.W, vector<int>(current_state.H, 0));

        rep(x, current_state.W) {
            rep(y, current_state.H) {
                if (StaticData::fields[x][y]) {
                    fixed_wrapped[x][y] = 1;
                }
            }
        }

        rep(x, current_state.W) {
            rep(y, current_state.H) {
                if (wrapped[x][y] == 1) {
                    fixed_wrapped[x][y] = 2;
                }
            }
        }

        dump_vec2(fixed_wrapped);
    }

    /**
     * 現在の状態を返す
     */
    State get_current_state() {
        return current_state;
    }

    /**
     * ラップしたマスの数を返す
     */
    int get_wrap_count() {
        return wrap_count;
    }

    /**
     * ラップする必要のあるマスの数を返す
     */
    int get_wrap_count_max() {
        return wrap_count_max;
    }

    /**
     * 全てをラップしたかを判定する
     */
    bool is_all_wrapped() {
        assert(wrap_count <= wrap_count_max);
        return wrap_count == wrap_count_max;
    }

    /**
     * p がラップされているか調べる
     */
    bool is_wrapped(Point p) {
        return wrapped[p.x][p.y];
    }

    /**
     * move_dir の方向にロボットを移動させる
     */
    Command move(int move_dir) {

        auto command = current_state.move(move_dir);
        wrap();
        trace.append(command);
        return command;
    }

    /**
     * マニピュレータを回転させる
     */
    Command turn(bool is_clockwise) {

        auto command = current_state.turn(is_clockwise);
        wrap();
        trace.append(command);
        return command;
    }

    /**
     * コマンドを実行する
     */
    void exec_command(const Command &command) {

        if (command.is_move_command) {
            move(command.move_dir);
        }

        if (command.is_turn_manipulators_command) {
            turn(command.is_clockwise);
        }

    }

    /**
     * 今まで実行してきた操作を出力する
     */
    void output_solution() {
        trace.output();
    }


};


Point read_point(string s) {

    if (s[0] == '(') {
        s = s.substr(1);
    }

    if (s[s.size() - 1] == ')') {
        s = s.substr(0, s.size() - 1);
    }

    auto v = split(s, ",");

    return Point(toint(v[0]), toint(v[1]));

}

vector<Point> convert_str_vec_2_point_vec(const vector<string> &v) {
    vector<Point> res;

    for (const auto &t: v) {
        res.push_back(read_point(t));
    }

    return res;
}


Point max_pos(vector<Point> &vp) {

    int max_y = 0;
    int max_x = 0;

    for (auto p: vp) {
        max_x = max(max_x, p.x);
        max_y = max(max_y, p.y);
    }

    return Point(max_x, max_y);
}

Point min_pos(vector<Point> &vp) {

    int min_y = INF;
    int min_x = INF;

    for (auto p: vp) {
        min_x = min(min_x, p.x);
        min_y = min(min_y, p.y);
    }

    return Point(min_x, min_y);
}

bool calc_area_dfs(Point pos, vector<vector<int>> &area, vector<vector<int>> &visited) {

    if (area[pos.x][pos.y] == 1) {
        return true;
    }

    if (pos.x == 0 || pos.y == 0 || pos.x == area.size() - 1 || pos.y == area[0].size() - 1) {
        return false;
    }

    if (visited[pos.x][pos.y]) {
        return true;
    }

    visited[pos.x][pos.y] = 1;

    bool ok = true;

    rep(i, 4) {
        int tx = pos.x + dx[i];
        int ty = pos.y + dy[i];

        ok = ok && calc_area_dfs(Point(tx, ty), area, visited);
    }

    return ok;
}

void fill_area(vector<Point> &v, vector<vector<int>> &area) {

    for (auto p: v) {
        rep(i, 8) {
            int tx = p.x + dx[i];
            int ty = p.y + dy[i];

            if (!check(tx, ty, area.size(), area[0].size())) {
                continue;
            }

            if (area[tx][ty] == 0) {
                vector<vector<int>> visited(area.size(), vector<int>(area[0].size(), 0));

                if (calc_area_dfs(Point(tx, ty), area, visited)) {

                    rep(x, area.size()) {
                        rep(y, area[0].size()) {
                            if (visited[x][y]) {
                                area[x][y] = 1;
                            }
                        }
                    }
                }
            }
        }
    }


}

/**
 * 頂点の集合から、エリアを計算する
 * 1 がエリア内, 0 がエリア外
 */
vector<vector<int>> calc_area(int W, int H, vector<Point> &v) {

    Point mp = max_pos(v);

    assert(mp.x > 0);
    assert(mp.y > 0);

    vector<vector<int>> area(W, vector<int>(H, 0));


    int n = v.size();

    rep(f, n + 1) {

        int i = f % n;
        int j = (i + 1) % n;
        int k = (j + 1) % n;

        assert (v[i].x == v[j].x || v[i].y == v[j].y);
        assert (v[j].x == v[k].x || v[j].y == v[k].y);

        if (v[i].x == v[j].x) {

            int x = v[i].x;
            int min_y = min(v[i].y, v[j].y);
            int max_y = max(v[i].y, v[j].y);

            if (v[i].y < v[j].y) {
                x--;
            }

            for (int y = min_y; y < max_y; y++) {
                area[x][y] = 1;
            }

        } else if (v[i].y == v[j].y) {

            int y = v[i].y;
            int min_x = min(v[i].x, v[j].x);
            int max_x = max(v[i].x, v[j].x);

            if (v[i].x > v[j].x) {
                y--;
            }

            for (int x = min_x; x < max_x; x++) {
                area[x][y] = 1;
            }
        }
    }

    fill_area(v, area);

    return area;

}


System read_desc(const char *file_name) {

    auto f = ifstream(file_name, ios::in | ios::binary);

    if (!f) {
        cerr << "ファイルが開けません" << endl;
        exit(1);
    }

    string input;
    f >> input;
    vector<string> vs = split(input, "#");

    string filed_size_str = vs[0];

    vector<Point> filed_size_v = convert_str_vec_2_point_vec(split(filed_size_str, "),("));

    int W = max_pos(filed_size_v).x;
    int H = max_pos(filed_size_v).y;

    StaticData::fields = vector<vector<int>>(W, vector<int>(H, 0));

    vector<vector<int>> area = calc_area(W, H, filed_size_v);

    rep(x, W) {
        rep(y, H) {
            if (area[x][y] == 1) {
                StaticData::fields[x][y] = 0;
            } else {
                StaticData::fields[x][y] = 1;
            }
        }
    }

    assert (min_pos(filed_size_v).x == 0);
    assert (min_pos(filed_size_v).y == 0);

    string init_pos_str = vs[1];

    auto init_pos = read_point(init_pos_str);

    string obstacle_str = vs[2];

    vector<string> obstacle_v = split(obstacle_str, ";");

    for (const auto &obstacle_str_2 : obstacle_v) {

        vector<Point> obstacle_v_2 = convert_str_vec_2_point_vec(split(obstacle_str_2, "),("));
        vector<vector<int>> obstacle_area = calc_area(W, H, obstacle_v_2);

        rep(x, obstacle_area.size()) {
            rep(y, obstacle_area[0].size()) {
                if (obstacle_area[x][y] == 1) {
                    StaticData::fields[x][y] = 1;
                }
            }
        }

    }

    string booster_str = vs[3];
    // とりあえず受け付けない
    // 必要になったら入れるようにする

    return System(W, H, init_pos);
}


System init_system(int argc, char *argv[]) {

    string filename = "../../../sample/prob-003.desc";

    bool enable_debug = true;

    if (argc >= 2) {
        filename = string(argv[1]);
    }
    if (argc >= 3) {
        string debug_str = string(argv[2]);
        if (debug_str == "--no-debug") {
            enable_debug = false;
        }
    }

    cerr << R"({ "file_name": ")" + filename + "\" }" << endl;

    init_define();
    System system = read_desc(filename.c_str());

    if (enable_debug) {
        system.debug();
    }

    return system;
}


#endif

