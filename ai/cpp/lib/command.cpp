#ifndef COMMAND_CPP
#define COMMAND_CPP

#include "define.cpp"

class Command {

    string command;


public:

    bool is_move_command;
    int move_dir;

    bool is_turn_manipulators_command;
    bool is_clockwise;

    Command() {}

    Command(string command)
            : command(std::move(command)),
              is_move_command(false),
              move_dir(-1),
              is_turn_manipulators_command(false),
              is_clockwise(false) {}

    static Command create_move_command(int move_dir) {

        Command command;

        if (move_dir == UP) { command = Command("W"); }
        if (move_dir == DOWN) { command = Command("S"); }
        if (move_dir == LEFT) { command = Command("A"); }
        if (move_dir == RIGHT) { command = Command("D"); }

        command.is_move_command = true;
        command.move_dir = move_dir;
        return command;
    }

    static Command create_turn_manipulators_command(bool is_clockwise) {

        Command command;

        if (is_clockwise) {
            command = Command("E");
        } else {
            command = Command("Q");
        }
        command.is_turn_manipulators_command = true;
        command.is_clockwise = is_clockwise;

        return command;
    }


    string to_string() {
        return command;
    }

};


#endif
