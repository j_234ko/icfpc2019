#ifndef COMMAND_CPP
#define COMMAND_CPP

#include "define.cpp"

class Command {


public:

    Command() {}

    string command;

    int idx;

    bool is_do_nothing_command;

    bool is_move_command;
    int move_dir;

    bool is_turn_manipulators_command;
    bool is_clockwise;

    bool is_cloning_command;

    bool is_extension_of_the_manipulator_command;
    int dx, dy;

    bool is_teleport_reset_command;
    bool is_teleport_shift_command;
    int beacon_id;

    bool is_drill_command;
    bool is_fast_wheels_command;

    explicit Command(int idx, string command)
            : idx(idx),
              command(std::move(command)),
              is_do_nothing_command(false),
              is_move_command(false),
              move_dir(-1),
              is_turn_manipulators_command(false),
              is_clockwise(false),
              is_cloning_command(false),
              is_extension_of_the_manipulator_command(false),
              dx(-1), dy(-1),
              is_teleport_reset_command(false),
              is_teleport_shift_command(false),
              beacon_id(-1),
              is_drill_command(false),
              is_fast_wheels_command(false) {}

    static Command create_move_command(int idx, int move_dir) {
        Command command;

        if (move_dir == UP) { command = Command(idx, "W"); }
        if (move_dir == DOWN) { command = Command(idx, "S"); }
        if (move_dir == LEFT) { command = Command(idx, "A"); }
        if (move_dir == RIGHT) { command = Command(idx, "D"); }

        command.is_move_command = true;
        command.move_dir = move_dir;
        return command;
    }

    static Command create_turn_manipulators_command(int idx, bool is_clockwise) {
        Command command;

        if (is_clockwise) {
            command = Command(idx, "E");
        } else {
            command = Command(idx, "Q");
        }
        command.is_turn_manipulators_command = true;
        command.is_clockwise = is_clockwise;

        return command;
    }

    static Command create_do_nothing(int idx) {
        Command command(idx, "Z");
        command.is_do_nothing_command = true;
        return command;
    }

    static Command create_fast_wheels_command(int idx) {
        Command command(idx, "F");
        command.is_fast_wheels_command = true;
        return command;
    }

    static Command create_drill_command(int idx) {
        Command command(idx, "L");
        command.is_drill_command = true;
        return command;
    }

    static Command create_reset_teleport_command(int idx) {
        Command command(idx, "R");
        command.is_teleport_reset_command = true;
        return command;
    }

    static Command create_shift_teleport_command(int idx, int beacon_id, int x, int y) {
        Command command(idx, "T(" + tostr(x) + "," + tostr(y) + ")");
        command.is_teleport_shift_command = true;
        command.dx = x;
        command.dy = y;
        command.beacon_id = beacon_id;
        return command;
    }

    static Command create_cloning_command(int idx) {
        Command command(idx, "C");
        command.is_cloning_command = true;

        return command;
    }

    static Command create_extension_of_the_manipulator_command(int idx, int dx, int dy) {
        Command command(idx, "B(" + tostr(dx) + "," + tostr(dy) + ")");
        command.is_extension_of_the_manipulator_command = true;
        command.dx = dx;
        command.dy = dy;

        return command;
    }


    string to_string() const {
        return command;
    }

};


#endif
