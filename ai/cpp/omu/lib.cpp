#ifndef LIB_CPP
#define LIB_CPP

#include <regex>
#include "define.cpp"
#include "command.cpp"


vector<string> split(string s, const string &t) {
    vector<string> ret;
    ret.emplace_back();

    int ts = t.size();
    rep(i, s.size()) {
        if (s.substr(i, ts) == t) {
            i += ts - 1;
            ret.emplace_back();
        } else {
            ret[ret.size() - 1] += s[i];
        }
    }

    if (ret[ret.size() - 1].empty()) ret.pop_back();
    return ret;
}

void dump_vec2(const vector<vector<int>> &v2) {

    int w = v2.size();
    int h = v2[0].size();

    dum(w)
    dump(h)

    for (int y = v2[0].size() - 1; y >= 0; y--) {
        for (int x = 0; x < v2.size(); x++) {
            cerr << v2[x][y];
        }
        cerr << endl;
    }
    cerr << endl;

}

class Trace {
    vector<Command> command_list;
    int turn;
    int created_turn;

public:

    Trace() : turn(0), created_turn(0) {}

    Trace(int created_turn) : turn(created_turn), created_turn(created_turn) {}

    void append(const Command &command) {
        command_list.push_back(command);
        turn += 1;
    }

    int get_turn() {
        return turn;
    }

    int get_created_turn() {
        return created_turn;
    }

    bool empty() {
        return command_list.empty();
    }

    vector<Command> get_command_list() {
        return command_list;
    }

    string calc_commands() {
        string commands;
        for (const auto &command: command_list) {
            commands += command.to_string();
        }
        return commands;
    }


    bool operator<(const Trace &other) const {
        return created_turn < other.created_turn;
    }

    bool operator==(const Trace &other) const {
        return created_turn == other.created_turn;
    }

};

class StaticData {
public:
    static vector<vector<int>> fields;
    static vector<vector<int>> wrapped;
    static vector<vector<int>> x_fields;
    static vector<vector<set<char>>> booster_hints;
};

/**
 * 障害物が無い: 0
 * 障害物がある: 1
 */
vector<vector<int>> StaticData::fields;

/**
 * ラップしていない: 0
 * ラップしている: 1
 */
vector<vector<int>> StaticData::wrapped;

/**
 * X でない: 0
 * X である: 1
 */
vector<vector<int>> StaticData::x_fields;

/**
 * コード値に対応したブースターが近くにある
 */
vector<vector<set<char>>> StaticData::booster_hints;


vector<Point> _calc_wrap(Point pos, const vector<Point> &wrap_range, int W, int H) {

    vector<Point> res;

    int wrap_count = 0;

    int up_look = 0;
    int down_look = 0;
    int right_look = 0;
    int left_look = 0;

    for (auto p : wrap_range) {

        int tx = pos.x + p.x;
        int ty = pos.y + p.y;

        if (!check(tx, ty, W, H)) {
            continue;
        }

        if (StaticData::fields[tx][ty] == 1) {
            continue;
        }

        if (p.x == 0) {
            if (p.y > 0) {

                if (p.y > up_look + 1) {
                    continue;
                }

                up_look = max(up_look, p.y);
            } else {

                if (p.y < down_look - 1) {
                    continue;
                }

                down_look = min(down_look, p.y);
            }
        }
        if (p.y == 0) {
            if (p.x > 0) {

                if (p.x > right_look + 1) {
                    continue;
                }

                right_look = max(right_look, p.x);
            } else {

                if (p.x < left_look - 1) {
                    continue;
                }

                left_look = min(left_look, p.x);
            }
        }

        if (!StaticData::wrapped[tx][ty]) {
            wrap_count += 1;
            res.emplace_back(tx, ty);
        }

    }

    return res;

}


int _wrap(Point pos, const vector<Point> &wrap_range, int W, int H, int wrap_count_max) {

    auto v = _calc_wrap(pos, wrap_range, W, H);
    for (auto p: v) {
        StaticData::wrapped[p.x][p.y] = 1;
    }
    return v.size();
}


class Booster {

public:

    char code;
    Point pos;
    bool is_manipulator;
    bool is_cloning;
    bool is_teleport;
    bool is_drill;
    bool is_fast_wheels;

    static const int MANIPULATOR_HINTS_RANGE = 30;
    static const int CLONING_HINTS_RANGE = INF;
    static const int TELEPORT_HINTS_RANGE = 10;
    static const int DRILL_HINTS_RANGE = 5;
    static const int FAST_WHEELS_HINTS_RANGE = 10;

    Booster(Point pos, char code)
            : pos(pos),
              code(code),
              is_manipulator(code == MANIPULATOR_CODE),
              is_cloning(code == CLONING_CODE),
              is_drill(code == DRILL_CODE),
              is_fast_wheels(code == FAST_WHEELS_CODE),
              is_teleport(code == TELEPORT_CODE) {
    }

    int get_hints_range() {
        if (is_manipulator) {
            return MANIPULATOR_HINTS_RANGE;
        }
        if (is_cloning) {
            return CLONING_HINTS_RANGE;
        }
        if (is_drill) {
            return DRILL_HINTS_RANGE;
        }
        if (is_fast_wheels) {
            return FAST_WHEELS_HINTS_RANGE;
        }
        if (is_teleport) {
            return TELEPORT_HINTS_RANGE;
        }
    }

};

class Robot {

public:

    int idx;
    int dir;
    Point pos;

    int booster_manipulator_count;
    int booster_cloning_count;
    int booster_teleport_count;
    int booster_drill_count;
    int booster_fast_wheels_count;

    int manipulator_size;

    int drill_activate_turn;
    int fast_wheels_activate_turn;


// fast_wheels を成立させる為のアドホックな変数
    bool use_fast_wheels;
    bool use_fast_wheels_of_state;

    Point fast_wheels_extend_pos;

    vector<Point> teleport_beacon_list;

    Robot(int idx, int dir, Point pos) :
            idx(idx),
            dir(dir),
            pos(pos),
            booster_manipulator_count(0),
            booster_cloning_count(0),
            booster_teleport_count(0),
            booster_drill_count(0),
            booster_fast_wheels_count(0),
            drill_activate_turn(0),
            fast_wheels_activate_turn(0),
            use_fast_wheels(false),
            use_fast_wheels_of_state(false),
            manipulator_size(4) {
    }

    void append_booster(Booster booster) {
        if (booster.is_manipulator) {
            booster_manipulator_count += 1;
        }
        if (booster.is_cloning) {
            booster_cloning_count += 1;
        }
        if (booster.is_teleport) {
            booster_teleport_count += 1;
        }
        if (booster.is_drill) {
            booster_drill_count += 1;
        }
        if (booster.is_fast_wheels) {
            booster_fast_wheels_count += 1;
        }
    }

    int count_booster_manipulator() const {
        return booster_manipulator_count;
    }

    Point use_booster_manipulator() {
        assert(booster_manipulator_count > 0);
        booster_manipulator_count--;
        manipulator_size += 1;
        return Point(get_wrap_range_list(dir, manipulator_size).back());
    }

    bool have_booster_manipulator() {
        return booster_manipulator_count > 0;
    }

    int count_booster_cloning() const {
        return booster_cloning_count;
    }

    void use_booster_cloning() {
        assert(booster_cloning_count > 0);
        booster_cloning_count--;
    }

    bool have_booster_cloning() {
        return booster_cloning_count > 0;
    }

    int count_booster_teleport() const {
        return booster_teleport_count;
    }

    void use_booster_teleport() {
        assert(booster_teleport_count > 0);
        teleport_beacon_list.push_back(pos);
        booster_teleport_count--;
    }

    bool have_booster_teleport() {
        return booster_teleport_count > 0;
    }

    int count_booster_drill() const {
        return booster_drill_count;
    }

    void use_booster_drill() {
        assert(booster_drill_count > 0);
        booster_drill_count--;
        if (fast_wheels_activate_turn) {
            drill_activate_turn += DRILL_ACTIVATE_TURN_MAX;
        } else {
            drill_activate_turn = DRILL_ACTIVATE_TURN_MAX + 1;
        }
    }

    bool have_booster_drill() {
        return booster_drill_count > 0;
    }

    int count_booster_fast_wheels() const {
        return booster_fast_wheels_count;
    }

    void use_booster_fast_wheels() {
        assert(booster_fast_wheels_count > 0);
        booster_fast_wheels_count--;
        if (fast_wheels_activate_turn) {
            fast_wheels_activate_turn += FAST_WHEELS_ACTIVATE_TURN_MAX;
        } else {
            fast_wheels_activate_turn = FAST_WHEELS_ACTIVATE_TURN_MAX + 1;
        }

    }

    bool have_booster_fast_wheels() {
        return booster_fast_wheels_count > 0;
    }

    bool is_activate_drill() const {
        return drill_activate_turn > 0;
    }

    bool is_activate_fast_wheels() const {
        return fast_wheels_activate_turn > 0;
    }

    void use_activate_turn() {

        if (drill_activate_turn > 0) {
            drill_activate_turn -= 1;
        }
        if (fast_wheels_activate_turn > 0) {
            fast_wheels_activate_turn -= 1;
        }

    }


};


class State {

private:

    int wrapped_count;


    void take_booster(int idx) {

        rep(jdx, booster_list.size()) {

            if (robot_list[idx].pos == booster_list[jdx].pos) {
                robot_list[idx].append_booster(booster_list[jdx]);
                booster_list.erase(booster_list.begin() + jdx);
                jdx--;

            }

        }


        // アドホックな処理
        if (robot_list[idx].use_fast_wheels_of_state) {
            rep(jdx, booster_list.size()) {

                if (robot_list[idx].fast_wheels_extend_pos == booster_list[jdx].pos) {
                    robot_list[idx].append_booster(booster_list[jdx]);
                    booster_list.erase(booster_list.begin() + jdx);
                    jdx--;
                }
            }

            robot_list[idx].use_fast_wheels_of_state = false;
        }


    }

    void use_activate_turn(int idx) {

        robot_list[idx].use_activate_turn();

    }

    void step(int idx) {

        take_booster(idx);
        use_activate_turn(idx);

    }


public:

    int W, H;

    vector<Robot> robot_list;
    vector<Booster> booster_list;

    State(int W,
          int H,
          int wrapped_count,
          vector<Robot> robot_list,
          vector<Booster> booster_list)
            : W(W),
              H(H),
              wrapped_count(wrapped_count),
              robot_list(std::move(robot_list)),
              booster_list(std::move(booster_list)) {}

    void debug(int idx) {

        auto &robot = robot_list[idx];

        cerr << "robot.idx: " + tostr(robot.idx) + "," +
                "robot.pos=(" + tostr(robot.pos.x) + "," + tostr(robot.pos.y) + ")," +
                "fast_wheel=" + tostr(robot.fast_wheels_activate_turn) + "," +
                "drill=" + tostr(robot.drill_activate_turn) << endl;
    }

    bool exists_cloning() {
        for (auto booster : booster_list) {
            if (booster.is_cloning) {
                return true;
            }
        }
        return false;
    }

    bool exists_manipulator() {
        for (auto booster : booster_list) {
            if (booster.is_manipulator) {
                return true;
            }
        }
        return false;
    }

    bool exists_drill() {
        for (auto booster : booster_list) {
            if (booster.is_drill) {
                return true;
            }
        }
        return false;
    }


    bool exists_teleport() {
        for (auto booster : booster_list) {
            if (booster.is_teleport) {
                return true;
            }
        }
        return false;
    }

    bool exists_fast_wheels() {
        for (auto booster : booster_list) {
            if (booster.is_fast_wheels) {
                return true;
            }
        }
        return false;
    }

    bool exists_teleport_beacon(Point pos) {

        for (auto &robot: robot_list) {
            for (auto &beacon_pos : robot.teleport_beacon_list) {
                if (beacon_pos == pos) {
                    return true;
                }
            }
        }

        return false;
    }

    vector<Point> get_wrap_point_list(int idx) {
        auto &robot = robot_list[idx];
        auto wrap_range = get_wrap_range_list(robot.dir, robot.manipulator_size);

        vector<Point> wrap_point_list;
        for (const auto &p: wrap_range) {
            int tx = p.x + robot.pos.x;
            int ty = p.y + robot.pos.y;

            if (!check(tx, ty, W, H)) {
                continue;
            }
            if (StaticData::fields[tx][ty] == 1) {
                continue;
            }

            wrap_point_list.emplace_back(tx, ty);
        }

        return wrap_point_list;
    }


    int count_manipulator_inside_obstacle(int idx) const {

        auto wrap_range = get_wrap_range_list(robot_list[idx].dir, robot_list[idx].manipulator_size);

        int count = 0;

        for (auto p: wrap_range) {

            int tx = robot_list[idx].pos.x + p.x;
            int ty = robot_list[idx].pos.y + p.y;

            if (!check(tx, ty, W, H)) {
                continue;
            }

            if (StaticData::fields[tx][ty] == 1) {
                count++;
            }
        }

        return count;
    }

    bool can_move_of_drill(int idx, int move_dir) const {

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        return tx >= 0 && ty >= 0 && tx < W && ty < H;

    }

    bool can_move_of_fast_wheels(int idx, int move_dir) const {

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        int tx2 = robot_list[idx].pos.x + dx[move_dir] * 2;
        int ty2 = robot_list[idx].pos.y + dy[move_dir] * 2;

        return tx >= 0 && ty >= 0 && tx < W && ty < H && StaticData::fields[tx][ty] == 0 &&
               tx2 >= 0 && ty2 >= 0 && tx2 < W && ty2 < H && StaticData::fields[tx2][ty2] == 0;
    }

    bool can_move_of_drill_and_fast_wheels(int idx, int move_dir) const {

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        int tx2 = robot_list[idx].pos.x + dx[move_dir] * 2;
        int ty2 = robot_list[idx].pos.y + dy[move_dir] * 2;

        return tx >= 0 && ty >= 0 && tx < W && ty < H &&
               tx2 >= 0 && ty2 >= 0 && tx2 < W && ty2 < H;
    }


    /**
     * move_dir の方向にロボットが移動可能か判定する
     */
    bool can_move(int idx, int move_dir) const {

        assert(idx < robot_list.size());

        if (robot_list[idx].is_activate_drill() && robot_list[idx].is_activate_fast_wheels()) {
            return can_move_of_drill_and_fast_wheels(idx, move_dir);
        }

        if (robot_list[idx].is_activate_drill()) {
            return can_move_of_drill(idx, move_dir);
        }

        if (robot_list[idx].is_activate_fast_wheels()) {
            return can_move_of_fast_wheels(idx, move_dir);
        }

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        return tx >= 0 && ty >= 0 && tx < W && ty < H && StaticData::fields[tx][ty] == 0;
    }

    Command do_nothing(int idx) {

        step(idx);

        return Command::create_do_nothing(idx);
    }

    Command move_of_drill(int idx, int move_dir) {
        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        assert (tx >= 0 && ty >= 0 && tx < W && ty < H);

        robot_list[idx].pos.x = tx;
        robot_list[idx].pos.y = ty;

        step(idx);

        return Command::create_move_command(idx, move_dir);
    }

    Command move_of_fast_wheels(int idx, int move_dir) {

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        // アドホックな処理
        robot_list[idx].use_fast_wheels = true;
        robot_list[idx].use_fast_wheels_of_state = true;

        robot_list[idx].fast_wheels_extend_pos = Point(tx, ty);

        int tx2 = robot_list[idx].pos.x + dx[move_dir] * 2;
        int ty2 = robot_list[idx].pos.y + dy[move_dir] * 2;

        assert (tx >= 0 && ty >= 0 && tx < W && ty < H);
        assert (StaticData::fields[tx][ty] == 0);

        assert (tx2 >= 0 && ty2 >= 0 && tx2 < W && ty2 < H);
        assert (StaticData::fields[tx2][ty2] == 0);

        robot_list[idx].pos.x = tx2;
        robot_list[idx].pos.y = ty2;

        step(idx);

        auto command = Command::create_move_command(idx, move_dir);

        return command;
    }

    Command move_of_drill_and_fast_wheels(int idx, int move_dir) {

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        // アドホックな処理
        robot_list[idx].use_fast_wheels = true;
        robot_list[idx].use_fast_wheels_of_state = true;
        robot_list[idx].fast_wheels_extend_pos = Point(tx, ty);

        int tx2 = robot_list[idx].pos.x + dx[move_dir] * 2;
        int ty2 = robot_list[idx].pos.y + dy[move_dir] * 2;

        assert (tx >= 0 && ty >= 0 && tx < W && ty < H);
        assert (tx2 >= 0 && ty2 >= 0 && tx2 < W && ty2 < H);

        robot_list[idx].pos.x = tx2;
        robot_list[idx].pos.y = ty2;

        step(idx);

        auto command = Command::create_move_command(idx, move_dir);

        return command;
    }

    /**
     * move_dir の方向にロボットを移動させる
     */
    Command move(int idx, int move_dir) {

        assert(idx < robot_list.size());

        if (robot_list[idx].is_activate_drill() && robot_list[idx].is_activate_fast_wheels()) {
            return move_of_drill_and_fast_wheels(idx, move_dir);
        }

        if (robot_list[idx].is_activate_drill()) {
            return move_of_drill(idx, move_dir);
        }

        if (robot_list[idx].is_activate_fast_wheels()) {
            return move_of_fast_wheels(idx, move_dir);
        }

        int tx = robot_list[idx].pos.x + dx[move_dir];
        int ty = robot_list[idx].pos.y + dy[move_dir];

        assert (tx >= 0 && ty >= 0 && tx < W && ty < H);
        assert (StaticData::fields[tx][ty] == 0);

        robot_list[idx].pos.x = tx;
        robot_list[idx].pos.y = ty;

        step(idx);

        return Command::create_move_command(idx, move_dir);
    }

    /**
     * マニピュレータを回転させる
     */
    Command turn(int idx, bool is_clockwise) {

        if (is_clockwise) {
            robot_list[idx].dir = (robot_list[idx].dir + 1) % 4;
        } else {
            robot_list[idx].dir = (robot_list[idx].dir + 3) % 4;
        }

        step(idx);

        return Command::create_turn_manipulators_command(idx, is_clockwise);
    }

    /**
     * マニピュレータの追加
     */
    Command extension_of_the_manipulator(int idx) {

        auto p = robot_list[idx].use_booster_manipulator();

        step(idx);

        return Command::create_extension_of_the_manipulator_command(idx, p.x, p.y);
    }

    /**
     * テレポート (ビーコンのインストール)
     */
    Command teleport_reset(int idx) {

        robot_list[idx].use_booster_teleport();

        step(idx);

        return Command::create_reset_teleport_command(idx);
    }

    /**
     * テレポート (ビーコンに移動)
     */
    Command teleport_shift(int idx, int beacon_idx) {

        Point teleport_pos = robot_list[idx].teleport_beacon_list[beacon_idx];

        robot_list[idx].pos = teleport_pos;

        step(idx);

        return Command::create_shift_teleport_command(idx, beacon_idx, teleport_pos.x, teleport_pos.y);
    }


    /**
     * クローン
     */
    Command cloning(int idx) {

        robot_list[idx].use_booster_cloning();
        robot_list.emplace_back(robot_list.size(), RIGHT, robot_list[idx].pos);

        step(idx);

        return Command::create_cloning_command(idx);
    }

    /**
     * ドリル
     */
    Command drill(int idx) {

        robot_list[idx].use_booster_drill();

        step(idx);

        return Command::create_drill_command(idx);
    }


    /**
     * ファストホイール
     */
    Command fast_wheels(int idx) {

        robot_list[idx].use_booster_fast_wheels();

        step(idx);

        return Command::create_fast_wheels_command(idx);
    }


    /**
     * コマンドを実行する
     */
    Command exec_command(const Command &command) {

        if (command.is_move_command) {
            return move(command.idx, command.move_dir);
        }

        if (command.is_turn_manipulators_command) {
            return turn(command.idx, command.is_clockwise);
        }

        if (command.is_do_nothing_command) {
            return do_nothing(command.idx);
        }

        if (command.is_fast_wheels_command) {
            return fast_wheels(command.idx);
        }

        if (command.is_drill_command) {
            return drill(command.idx);
        }

        if (command.is_extension_of_the_manipulator_command) {
            return extension_of_the_manipulator(command.idx);
        }

        if (command.is_cloning_command) {
            return cloning(command.idx);
        }

        if (command.is_teleport_reset_command) {
            return teleport_reset(command.idx);
        }

        if (command.is_teleport_shift_command) {
            return teleport_shift(command.idx, command.beacon_id);
        }

        assert (0);
    }

    /**
     * 移動可能な方向を列挙する
     */
    vector<int> move_dir_list(int idx) const {

        vector<int> res;

        rep(move_dir, 4) {
            if (can_move(idx, move_dir)) {
                res.push_back(move_dir);
            }
        }

        return res;
    }

    bool can_wrapped(int idx) {

        auto &robot = robot_list[idx];
        auto vp = _calc_wrap(robot.pos,
                             get_wrap_range_list(robot.dir,
                                                 robot.manipulator_size),
                             W,
                             H);
        return vp.size();
    }

    /**
     * 繊維可能な状態を列挙する
     * pair.first = その状態に遷移するためのコマンド
     * pair.second = 遷移後の状態
     */
    vector<pair<Command, State>> next_command_to_state_list(int idx) {

        vector<int> dir_list = move_dir_list(idx);

        vector<pair<Command, State>> state_list;


        for (int i = 0; i < dir_list.size(); i++) {
            int j = (idx + i) % dir_list.size();

            State next_state = *this;
            auto command = next_state.move(idx, dir_list[j]);
            state_list.emplace_back(command, next_state);

        }


        rep(is_clockwise, 2) {

            State next_state = *this;
            auto command = next_state.turn(idx, is_clockwise);
            state_list.emplace_back(command, next_state);

        }

        rep(teleport_beacon_idx, robot_list[idx].teleport_beacon_list.size()) {

            State next_state = *this;
            auto command = next_state.teleport_shift(idx, teleport_beacon_idx);
            state_list.emplace_back(command, next_state);

        }

        return state_list;
    }

    /**
     * 繊維可能な状態を列挙する
     * pair.first = その状態に遷移するためのコマンド
     * pair.second = 遷移後の状態
     */
    vector<pair<Command, State>> next_command_to_state_list_move_only(int idx) const {

        vector<int> dir_list = move_dir_list(idx);

        vector<pair<Command, State>> state_list;

        for (int i = 0; i < dir_list.size(); i++) {
            int j = (idx + i) % dir_list.size();

            State next_state = *this;
            auto command = next_state.move(idx, dir_list[j]);
            state_list.emplace_back(command, next_state);

        }

        return state_list;
    }


};


class System {

private:

    Point init_pos;
    vector<Trace> trace_list;
    State current_state;
    int wrap_count;
    int wrap_count_max;

    void wrap(int idx) {
        auto &robot = current_state.robot_list[idx];
        wrap_count += _wrap(robot.pos,
                            get_wrap_range_list(robot.dir, robot.manipulator_size),
                            current_state.W,
                            current_state.H,
                            wrap_count_max);

        // アドホックな処理
        if (current_state.robot_list[idx].use_fast_wheels) {

            wrap_count += _wrap(current_state.robot_list[idx].fast_wheels_extend_pos,
                                get_wrap_range_list(robot.dir, robot.manipulator_size),
                                current_state.W,
                                current_state.H,
                                wrap_count_max);

            current_state.robot_list[idx].use_fast_wheels = false;
        }
    }

public:

    bool enable_debug;
    bool enable_force_debug;

    System(int W,
           int H,
           int wrap_count,
           int wrap_count_max,
           vector<Robot> robot_list,
           vector<Booster> booster_list)
            : current_state(W,
                            H,
                            wrap_count,
                            std::move(robot_list),
                            std::move(booster_list)),
              wrap_count(wrap_count),
              wrap_count_max(wrap_count_max),
              trace_list(1, Trace()),
              enable_debug(false),
              enable_force_debug(false) {

    }

    void debug(int idx) {

        cerr << "-----" << endl;
        current_state.debug(idx);

        if (enable_force_debug) {
            debug_wrapped(idx);

        }

    }

    void debug_init() {

        int W = current_state.W;
        int H = current_state.H;

        dum(W)
        dump(H)

        dum(init_pos.x)
        dump(init_pos.y)

        dump_vec2(StaticData::fields);
    }

    void debug_wrapped(int idx) {

        vector<vector<int>> fixed_wrapped(current_state.W, vector<int>(current_state.H, 0));

        dum(wrap_count)
        dump(wrap_count_max)

        rep(x, current_state.W) {
            rep(y, current_state.H) {
                if (StaticData::fields[x][y]) {
                    fixed_wrapped[x][y] = 1;
                }
            }
        }

        rep(x, current_state.W) {
            rep(y, current_state.H) {
                if (StaticData::wrapped[x][y] == 1) {
                    fixed_wrapped[x][y] = 2;
                }
            }
        }

        for (const auto &robot : current_state.robot_list) {
            if (robot.idx == idx) {
                fixed_wrapped[robot.pos.x][robot.pos.y] = 9;
            } else {
                fixed_wrapped[robot.pos.x][robot.pos.y] = 6;
            }
        }

        dump_vec2(fixed_wrapped);
    }

    int get_turn(int idx) {
        return trace_list[idx].get_turn();
    }

    int get_created_turn(int idx) {
        return trace_list[idx].get_created_turn();
    }

    /**
     * 現在の状態を返す
     */
    State get_current_state() {
        return current_state;
    }

    /**
     * ラップしたマスの数を返す
     */
    int get_wrap_count() {
        return wrap_count;
    }

    /**
     * ラップする必要のあるマスの数を返す
     */
    int get_wrap_count_max() {
        return wrap_count_max;
    }

    /**
     * 全てをラップしたかを判定する
     */
    bool is_all_wrapped() {
        assert(wrap_count <= wrap_count_max);
        return wrap_count == wrap_count_max;
    }

    /**
     * p がラップされているか調べる
     */
    bool is_wrapped(Point p) const {
        return StaticData::wrapped[p.x][p.y];
    }

    /**
     * p が障害物か調べる
     */
    bool is_obstacle(Point p) {
        return StaticData::fields[p.x][p.y];
    }

    /**
     * p が X か調べる
     */
    bool is_x_field(Point p) {
        return StaticData::x_fields[p.x][p.y];
    }

    bool exists_manipulator_hints(Point p) {
        return StaticData::booster_hints[p.x][p.y].count(MANIPULATOR_CODE);
    }

    bool exists_fast_wheels_hints(Point p) {
        return StaticData::booster_hints[p.x][p.y].count(FAST_WHEELS_CODE);
    }

    bool exists_drill_hints(Point p) {
        return StaticData::booster_hints[p.x][p.y].count(DRILL_CODE);
    }

    bool exists_teleport_hints(Point p) {
        return StaticData::booster_hints[p.x][p.y].count(TELEPORT_CODE);
    }

    bool exists_cloning_hints(Point p) {
        return StaticData::booster_hints[p.x][p.y].count(CLONING_CODE);
    }


    /**
     * 回転処理
     */
    Command turn(int idx, bool is_clockwise) {

        auto command = current_state.turn(idx, is_clockwise);
        wrap(idx);
        trace_list[idx].append(command);

        if (enable_debug) {
            debug(idx);
            cerr << "command: turn (idx=" + tostr(idx) + ",is_clockwise=" + tostr(is_clockwise) + ")" << endl;
        }

        return command;
    }

    /**
     * 何もしない
     */
    Command do_nothing(int idx) {

        auto command = current_state.do_nothing(idx);
        wrap(idx);
        trace_list[idx].append(command);

        if (enable_debug) {
            debug(idx);
            cerr << "command: do nothing (idx=" + tostr(idx) + ")" << endl;
        }

        return command;
    }


    /**
     * ドリル
     */
    Command drill(int idx) {

        auto command = current_state.drill(idx);
        wrap(idx);
        trace_list[idx].append(command);

        if (enable_debug) {
            debug(idx);
            cerr << "command: drill command (idx=" + tostr(idx) + ")" << endl;
        }

        return command;
    }

    /**
     * ファストホイール
     */
    Command fast_wheels(int idx) {

        auto command = current_state.fast_wheels(idx);
        wrap(idx);
        trace_list[idx].append(command);

        if (enable_debug) {
            debug(idx);
            cerr << "command: fast wheels command (idx=" + tostr(idx) + ")" << endl;
        }

        return command;
    }


    /**
     * マニピュレータの追加
     */
    Command extension_of_the_manipulator(int idx) {

        auto command = current_state.extension_of_the_manipulator(idx);
        wrap(idx);
        trace_list[idx].append(command);

        if (enable_debug) {
            debug(idx);
            cerr << "command: extension manipulator command (idx=" + tostr(idx) + ")" << endl;
        }

        return command;
    }

    /**
     * テレポート (ビーコンのインストール)
     */
    Command teleport_reset(int idx) {

        assert(!is_x_field(current_state.robot_list[idx].pos));
        auto command = current_state.teleport_reset(idx);
        wrap(idx);
        trace_list[idx].append(command);


        if (enable_debug) {
            debug(idx);
            cerr << "command: teleport reset command (idx=" + tostr(idx) + ")" << endl;
        }

        return command;
    }

    /**
     * テレポート (ビーコンに移動)
     */
    Command teleport_shift(int idx, int beacon_idx) {

        auto command = current_state.teleport_shift(idx, beacon_idx);
        wrap(idx);
        trace_list[idx].append(command);

        if (enable_debug) {
            debug(idx);
            cerr << "command: teleport shift command "
                    "(idx=" + tostr(idx) + ",x=" + tostr(command.dx) + ",y=" + tostr(command.dy) + ")" << endl;
        }

        return command;
    }


    /**
     * クローン
     */
    Command cloning(int idx) {

        auto command = current_state.cloning(idx);
        wrap(idx);
        trace_list[idx].append(command);
        trace_list.emplace_back(trace_list[idx].get_turn());

        if (enable_debug) {
            debug(idx);
            cerr << "command: cloning command (idx: " + tostr(idx) + ")" << endl;
        }

        return command;
    }


    /**
     * コマンドを実行する
     */
    Command exec_command(const Command &command) {

        if (enable_debug) {
            debug(command.idx);
            cerr << "command: " + command.to_string() << endl;
        }

        current_state.exec_command(command);
        wrap(command.idx);
        trace_list[command.idx].append(command);


        return command;

    }


    /**
     * 今まで実行してきた操作を出力する
     */
    pair<int, string> calc_solution() {

        string commands = calc_commands();

        string fixed_commands = commands;

        regex re("\\([0-9-,]+\\)");
        fixed_commands = regex_replace(fixed_commands, re, "");

        auto v = split(split(fixed_commands, "#")[0], "C");
        int score = v[0].size();

        if (v.size() >= 2) {
            auto v2 = split(commands, "#");

            score += v.size() - 1;
            int t = 0;
            for (const auto &s: v2) {
                t = max(t, int(split(s, "C").back().size()));
            }

            score += t;
        }
        cerr << R"({ "score": ")" + tostr(score) + "\" }" << endl;

        return make_pair(score, commands);

    }


    string calc_commands() {

        string commands;

        sort(all(trace_list));

        for (int idx = 0; idx < trace_list.size(); idx++) {
            if (idx >= 1) {
                commands += "#";
            }
            commands += trace_list[idx].calc_commands();
        }
        return commands;

    }

};


Point read_point(string s) {

    if (s[0] == '(') {
        s = s.substr(1);
    }

    if (s[s.size() - 1] == ')') {
        s = s.substr(0, s.size() - 1);
    }

    auto v = split(s, ",");

    return Point(toint(v[0]), toint(v[1]));

}

vector<Point> convert_str_vec_2_point_vec(const vector<string> &v) {
    vector<Point> res;

    for (const auto &t: v) {
        res.push_back(read_point(t));
    }

    return res;
}


Point max_pos(vector<Point> &vp) {

    int max_y = 0;
    int max_x = 0;

    for (auto p: vp) {
        max_x = max(max_x, p.x);
        max_y = max(max_y, p.y);
    }

    return Point(max_x, max_y);
}

Point min_pos(vector<Point> &vp) {

    int min_y = INF;
    int min_x = INF;

    for (auto p: vp) {
        min_x = min(min_x, p.x);
        min_y = min(min_y, p.y);
    }

    return Point(min_x, min_y);
}

bool calc_area_dfs(Point pos, vector<vector<int>> &area, vector<vector<int>> &visited) {

    if (area[pos.x][pos.y] == 1) {
        return true;
    }

    if (pos.x == 0 || pos.y == 0 || pos.x == area.size() - 1 || pos.y == area[0].size() - 1) {
        return false;
    }

    if (visited[pos.x][pos.y]) {
        return true;
    }

    visited[pos.x][pos.y] = 1;

    bool ok = true;

    rep(i, 4) {
        int tx = pos.x + dx[i];
        int ty = pos.y + dy[i];

        ok = ok && calc_area_dfs(Point(tx, ty), area, visited);
    }

    return ok;
}

void fill_area(vector<Point> &v, vector<vector<int>> &area) {

    vector<vector<int>> visited(area.size(), vector<int>(area[0].size(), 0));

    queue<Point> que;

    for (int x = 0; x < area.size(); x++) {
        for (int y = 0; y < area[0].size(); y += area[0].size() - 1) {
            if (area[x][y] == 0) {
                que.push(Point(x, y));
                visited[x][y] = 1;
            }
        }
    }
    for (int y = 0; y < area[0].size(); y++) {
        for (int x = 0; x < area.size(); x += area.size() - 1) {
            if (area[x][y] == 0) {
                que.push(Point(x, y));
                visited[x][y] = 1;
            }
        }
    }

    while (!que.empty()) {

        auto p = que.front();

        que.pop();


        rep(i, 4) {

            int tx = p.x + dx[i];
            int ty = p.y + dy[i];

            if (!check(tx, ty, area.size(), area[0].size())) {
                continue;
            }

            if (area[tx][ty] == 1) {
                continue;
            }

            if (!visited[tx][ty]) {

                visited[tx][ty] = 1;
                que.push(Point(tx, ty));

            }

        }
    }

    rep(x, area.size()) {
        rep(y, area[0].size()) {
            if (visited[x][y] == 1) {
                area[x][y] = 0;
            } else {
                area[x][y] = 1;
            }
        }
    }

}

/**
 * 頂点の集合から、エリアを計算する
 * 1 がエリア内, 0 がエリア外
 */
vector<vector<int>> calc_area(int W, int H, vector<Point> &v) {

    Point mp = max_pos(v);

    assert(mp.x > 0);
    assert(mp.y > 0);

    vector<vector<int>> area(W, vector<int>(H, 0));


    int n = v.size();

    rep(f, n + 1) {

        int i = f % n;
        int j = (i + 1) % n;
        int k = (j + 1) % n;

        assert (v[i].x == v[j].x || v[i].y == v[j].y);
        assert (v[j].x == v[k].x || v[j].y == v[k].y);

        if (v[i].x == v[j].x) {

            int x = v[i].x;
            int min_y = min(v[i].y, v[j].y);
            int max_y = max(v[i].y, v[j].y);

            if (v[i].y < v[j].y) {
                x--;
            }

            for (int y = min_y; y < max_y; y++) {
                area[x][y] = 1;
            }

        } else if (v[i].y == v[j].y) {

            int y = v[i].y;
            int min_x = min(v[i].x, v[j].x);
            int max_x = max(v[i].x, v[j].x);

            if (v[i].x > v[j].x) {
                y--;
            }

            for (int x = min_x; x < max_x; x++) {
                area[x][y] = 1;
            }
        }
    }

    fill_area(v, area);

    return area;

}

void set_booster_hints(int W, int H, Booster booster) {

    int min_x = max(0, booster.pos.x - booster.get_hints_range());
    int min_y = max(0, booster.pos.y - booster.get_hints_range());

    int max_x = min(W, booster.pos.x + booster.get_hints_range());
    int max_y = min(H, booster.pos.y + booster.get_hints_range());

    for (int x = min_x; x < max_x; x++) {
        for (int y = min_y; y < max_y; y++) {
            StaticData::booster_hints[x][y].insert(booster.code);
        }
    }

}


System read_desc(const char *file_name) {

    auto f = ifstream(file_name, ios::in | ios::binary);

    if (!f) {
        cerr << "ファイルが開けません" << endl;
        exit(1);
    }

    string input;
    f >> input;
    vector<string> vs = split(input, "#");

    string filed_size_str = vs[0];

    vector<Point> filed_size_v = convert_str_vec_2_point_vec(split(filed_size_str, "),("));

    int W = max_pos(filed_size_v).x;
    int H = max_pos(filed_size_v).y;

    StaticData::fields = vector<vector<int>>(W, vector<int>(H, 0));
    StaticData::wrapped = vector<vector<int>>(W, vector<int>(H, 0));
    StaticData::x_fields = vector<vector<int>>(W, vector<int>(H, 0));
    StaticData::booster_hints = vector<vector<set<char>>>(W, vector<set<char>>(H, set<char>()));

    vector<vector<int>> area = calc_area(W, H, filed_size_v);

    rep(x, W) {
        rep(y, H) {
            if (area[x][y] == 1) {
                StaticData::fields[x][y] = 0;
            } else {
                StaticData::fields[x][y] = 1;
            }
        }
    }

    string init_pos_str = vs[1];

    auto init_pos = read_point(init_pos_str);

    string obstacle_str = vs[2];

    vector<string> obstacle_v = split(obstacle_str, ";");

    for (const auto &obstacle_str_2 : obstacle_v) {

        vector<Point> obstacle_v_2 = convert_str_vec_2_point_vec(split(obstacle_str_2, "),("));
        vector<vector<int>> obstacle_area = calc_area(W, H, obstacle_v_2);

        rep(x, obstacle_area.size()) {
            rep(y, obstacle_area[0].size()) {
                if (obstacle_area[x][y] == 1) {
                    StaticData::fields[x][y] = 1;
                }
            }
        }

    }

    int wrap_count_max = 0;

    rep(x, W) {
        rep(y, H) {
            if (StaticData::fields[x][y] == 0) {
                wrap_count_max += 1;
            }
        }
    }


    int wrap_count = _wrap(init_pos, get_wrap_range_list(RIGHT, 4), W, H, wrap_count_max);

    string booster_str = vs[3];

    auto booster_v = split(booster_str, ";");

    vector<Booster> booster_list;

    for (auto booster_s : booster_v) {

        char code = booster_s[0];
        booster_s = booster_s.substr(1);
        Point pos = read_point(booster_s);
        if (code == X_CODE) {
            StaticData::x_fields[pos.x][pos.y] = 1;
        } else {
            booster_list.emplace_back(pos, code);
            set_booster_hints(W, H, booster_list.back());

        }

    }

    Robot init_robot(0, RIGHT, init_pos);

    return System(W, H, wrap_count, wrap_count_max, vector<Robot>(1, init_robot), booster_list);
}

class InitialConfig {
public:


    string file_name;

    bool enable_output_local_file;
    bool enable_debug;
    bool enable_force_debug;

    explicit InitialConfig(string file_name) :
            file_name(std::move(file_name)),
            enable_output_local_file(false),
            enable_debug(true),
            enable_force_debug(false) {

    }
};

System init_system(const InitialConfig &config) {


    cerr << R"({ "file_name": ")" + config.file_name + "\" }" << endl;

    init_define();

    System system = read_desc(config.file_name.c_str());

    system.enable_debug = config.enable_debug;
    system.enable_force_debug = config.enable_force_debug;

    if (system.enable_debug) {
        system.debug_init();
    }

    return system;
}


#endif

