#include "lib.cpp"


class StateWrapper {

    Trace trace;
    State state;
    State init_state;

public:

    explicit StateWrapper(State init_state, State state, Trace trace)
            : init_state(std::move(init_state)),
              state(std::move(state)),
              trace(std::move(trace)) {}

    State get_state() {
        return state;
    }

    Trace get_trace() {
        return trace;
    }


};

Trace bfs(int idx, System &sys, const State &init_state) {

    vector<vector<vector<int>>> visited(init_state.W, vector<vector<int>>(init_state.H, vector<int>(4, 0)));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    auto &init_robot = init_state.robot_list[idx];
    visited[init_robot.pos.x][init_robot.pos.y][init_robot.dir] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            auto wrap_point_list = next_state.get_wrap_point_list(idx);

            auto &robot = next_state.robot_list[idx];

            if (next_state.can_wrapped(idx)) {

                return next_trace;

            } else if (!visited[robot.pos.x][robot.pos.y][robot.dir]) {

                visited[robot.pos.x][robot.pos.y][robot.dir] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}


Trace manipulator_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int manipulator_count = init_state.robot_list[idx].count_booster_manipulator();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_manipulator() > manipulator_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}

Trace cloning_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int cloning_count = init_state.robot_list[idx].count_booster_cloning();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_cloning() > cloning_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}


Trace x_fields_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (sys.is_x_field(next_state.robot_list[idx].pos)) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();

}


Trace teleport_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int teleport_count = init_state.robot_list[idx].count_booster_teleport();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_teleport() > teleport_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();
}

Trace fast_wheels_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int fast_wheels_count = init_state.robot_list[idx].count_booster_fast_wheels();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_fast_wheels() > fast_wheels_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();
}

Trace drill_bfs(int idx, System &sys, const State &init_state) {

    vector<vector<int>> visited(init_state.W, vector<int>(init_state.H, 0));

    queue<StateWrapper> que;
    que.push(StateWrapper(init_state, init_state, Trace()));
    visited[init_state.robot_list[idx].pos.x][init_state.robot_list[idx].pos.y] = 1;

    int drill_count = init_state.robot_list[idx].count_booster_drill();

    while (!que.empty()) {

        auto state_wrapper = que.front();
        que.pop();

        auto current_state = state_wrapper.get_state();
        auto trace = state_wrapper.get_trace();

        auto next_command_to_state_list = current_state.next_command_to_state_list_move_only(idx);

        for (const auto &next_command_to_state : next_command_to_state_list) {

            auto next_command = next_command_to_state.first;
            auto next_state = next_command_to_state.second;

            auto next_trace = trace;

            next_trace.append(next_command);

            if (next_state.robot_list[idx].count_booster_drill() > drill_count) {

                return next_trace;

            } else if (!visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y]) {

                visited[next_state.robot_list[idx].pos.x][next_state.robot_list[idx].pos.y] = 1;
                auto next_state_wrapper = StateWrapper(init_state, next_state, next_trace);
                que.push(next_state_wrapper);

            }
        }

    }

    return Trace();
}


bool step(System &system,
          bool enable_first_step_clockwise_turn) {

    int min_turn = INF;
    int max_turn = 0;

    for (int idx = 0; idx < system.get_current_state().robot_list.size(); idx++) {
        min_turn = min(min_turn, system.get_turn(idx));
        max_turn = max(max_turn, system.get_turn(idx));
    }


    for (int idx = 0; idx < system.get_current_state().robot_list.size(); idx++) {

        // 全てラップしていれば終了
        if (system.is_all_wrapped()) {
            return true;
        }

        if (min_turn != system.get_turn(idx)) {
            continue;
        }

        if (enable_first_step_clockwise_turn && system.get_created_turn(idx) == system.get_turn(idx)) {
            system.turn(idx, true);
            continue;
        }

        // beacon がなく x フィールドにおらず テレポートを所持していれば使う
        if (!system.get_current_state().exists_teleport_beacon(system.get_current_state().robot_list[idx].pos) &&
            !system.is_x_field(system.get_current_state().robot_list[idx].pos) &&
            system.get_current_state().robot_list[idx].have_booster_teleport()) {
            system.teleport_reset(idx);

            if (system.enable_debug) {
                cerr << "mode: use boost teleport" << endl;
            }
            continue;
        }

//        // ドリルを所持していれば使う
//        if (system.get_current_state().robot_list[idx].have_booster_drill()) {
//            system.drill(idx);
//            if (system.enable_debug) {
//                cerr << "mode: use boost drill" << endl;
//            }
//            continue;
//        }

        // マニピュレータを所持していれば使う
        if (system.get_current_state().robot_list[idx].have_booster_manipulator()) {
            system.extension_of_the_manipulator(idx);
            if (system.enable_debug) {
                cerr << "mode: use boost manipulator" << endl;
            }
            continue;
        }

        // x フィールドにいて cloning があれば clone
        if (system.is_x_field(system.get_current_state().robot_list[idx].pos) &&
            system.get_current_state().robot_list[idx].have_booster_cloning()) {
            system.cloning(idx);
            idx--;
            if (system.enable_debug) {
                cerr << "mode: use boost cloning" << endl;
            }
            continue;
        }

        if (!system.get_current_state().robot_list[idx].is_activate_fast_wheels()) {

            if (system.exists_fast_wheels_hints(system.get_current_state().robot_list[idx].pos) &&
                system.get_current_state().exists_fast_wheels()) {
                // fast_wheels を探す
                Trace trace = fast_wheels_bfs(idx, system, system.get_current_state());
                assert (!trace.empty());
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                if (system.enable_debug) {
                    cerr << "mode: search boost fast_wheels" << endl;
                }
                continue;
            }

            if (system.exists_manipulator_hints(system.get_current_state().robot_list[idx].pos) &&
                system.get_current_state().exists_manipulator()) {

                // マニピュレータを探す
                Trace trace = manipulator_bfs(idx, system, system.get_current_state());
                assert (!trace.empty());
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                if (system.enable_debug) {
                    cerr << "mode: search boost manipulator" << endl;
                }
                continue;
            }

            // cloning があれば X を探す
            if (system.get_current_state().robot_list[idx].have_booster_cloning()) {
                Trace trace = x_fields_bfs(idx, system, system.get_current_state());

                assert(!trace.empty());
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                if (system.enable_debug) {
                    cerr << "mode: search point X" << endl;
                }
                continue;
            }

            if (system.exists_cloning_hints(system.get_current_state().robot_list[idx].pos) &&
                system.get_current_state().exists_cloning()) {
                // cloning を探す
                Trace trace = cloning_bfs(idx, system, system.get_current_state());
                assert (!trace.empty());
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                if (system.enable_debug) {
                    cerr << "mode: search boost cloning" << endl;
                }
                continue;
            }


            if (system.exists_teleport_hints(system.get_current_state().robot_list[idx].pos) &&
                system.get_current_state().exists_teleport()) {

                // teleport を探す
                Trace trace = teleport_bfs(idx, system, system.get_current_state());
                assert (!trace.empty());
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                if (system.enable_debug) {
                    cerr << "mode: search boost teleport" << endl;
                }
                continue;
            }

//            if (system.exists_drill_hints(system.get_current_state().robot_list[idx].pos) &&
//                system.get_current_state().exists_drill()) {
//                // drill を探す
//                Trace trace = drill_bfs(idx, system, system.get_current_state());
//                assert (!trace.empty());
//                for (const auto &command: trace.get_command_list()) {
//                    system.exec_command(command);
//                }
//                if (system.enable_debug) {
//                    cerr << "mode: search boost drill" << endl;
//                }
//                continue;
//            }

        }

        // ファストホイールを所持していれば使う
        if (!system.get_current_state().robot_list[idx].is_activate_fast_wheels() &&
            system.get_current_state().robot_list[idx].have_booster_fast_wheels()) {
            system.fast_wheels(idx);
            if (system.enable_debug) {
                cerr << "mode: use boost fast_wheels" << endl;
            }
            continue;
        }

        {
            Trace trace = bfs(idx, system, system.get_current_state());

            if (!trace.empty()) {
                for (const auto &command: trace.get_command_list()) {
                    system.exec_command(command);
                }
                if (system.enable_debug) {
                    cerr << "mode: search point unwrap" << endl;
                }
                continue;
            }


            if (system.get_current_state().robot_list[idx].is_activate_fast_wheels() &&
                trace.empty() && !system.is_all_wrapped()) {
                system.do_nothing(idx);
                if (system.enable_debug) {
                    cerr << "mode: wait expire fast_wheels" << endl;
                }
                continue;
            }
        }

        {
            ofstream ofs("../../../out_dump.sol");
            ofs << system.calc_solution().second;
            ofs.close();
        }

        assert(system.is_all_wrapped());
    }

    return false;
}

pair<int, string> sub(const InitialConfig &config, bool enable_first_step_clockwise_turn) {

    System system = init_system(config);

    while (true) {
        bool is_solved_problem = step(system,
                                      enable_first_step_clockwise_turn);
        if (is_solved_problem) break;
    }

    return system.calc_solution();

}

int main(int argc, char *argv[]) {

    pair<int, string> best_solution(INF, "solver can not calculate the answer");

    InitialConfig config("../../../tasks/prob-051.desc");
//    InitialConfig config("../../../tasks/prob-100.desc");

    for (int i = 1; i < argc; i++) {
        auto arg = string(argv[i]);
        if (arg == "--local") {
            config.enable_output_local_file = true;
        } else if (arg == "--no-debug") {
            config.enable_debug = false;
        } else if (arg == "--force-debug") {
            config.enable_force_debug = true;
        } else {
            config.file_name = arg;
        }
    }

    if (config.enable_output_local_file) {
        auto f = ifstream(config.file_name, ios::in | ios::binary);
        string s;
        f >> s;
        ofstream ofs("../../../in.desc");
        ofs << s;
        ofs.close();
    }

    best_solution = min(best_solution, sub(config, true));
    best_solution = min(best_solution, sub(config, false));

    if (config.enable_output_local_file) {

        ofstream ofs("../../../out.sol");
        ofs << best_solution.second;
        ofs.close();

    } else {

        cout << best_solution.second << endl;

    }


}