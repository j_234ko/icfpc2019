#include "lib.cpp"


class StateWrapper {

    Trace trace;
    State state;
    State init_state;

    int value;

public:

    explicit StateWrapper(State init_state, State state, Trace trace)
            : init_state(std::move(init_state)),
              state(std::move(state)),
              trace(std::move(trace)),
              value(calc_value()) {}

    State get_state() {
        return state;
    }

    Trace get_trace() {
        return trace;
    }

    int calc_value() {
        int _value = 0;

        _value += state.get_wrapped_count() * 10;
        _value += init_state.pos.dist(state.pos);

        return _value;
    }

    bool operator<(const StateWrapper &other) const {
        return value < other.value;
    }

};


Trace beam_search(System &sys, const State &init_state, int max_depth, int beam_width) {

    vector<priority_queue<StateWrapper>> que(max_depth + 1);
    que[0].push(StateWrapper(init_state, init_state, Trace()));

    Trace best_trace;

    for (int depth = 0; depth < max_depth; depth++) {
        for (int i = 0; i < beam_width; i++) {

            if (que[depth].empty()) {
                break;
            }

            auto state_wrapper = que[depth].top();
            que[depth].pop();

            auto current_state = state_wrapper.get_state();
            auto trace = state_wrapper.get_trace();

            auto next_command_to_state_list = current_state.next_command_to_state_list();

            for (const auto &next_command_to_state : next_command_to_state_list) {

                auto next_command = next_command_to_state.first;
                auto next_state = next_command_to_state.second;

                auto next_trace = trace;

                next_trace.append(next_command);

                if (!que[depth + 1].empty()) {
                    auto top = que[depth + 1].top();
                    best_trace = top.get_trace();
                }

                que[depth + 1].push(StateWrapper(init_state, next_state, next_trace));
            }

        }
    }

    return best_trace;
}

int main(int argc, char *argv[]) {

    System system = init_system(argc, argv);

    while (true) {

        if (system.is_all_wrapped()) {
            break;
        }

        Trace trace = beam_search(system, system.get_current_state(), 70, 300);

        if (trace.empty()) {

            assert(system.is_all_wrapped());
            break;

        }

        for (const auto &command: trace.get_command_list()) {
            system.exec_command(command);
        }

        system.get_current_state().debug();
        system.debug_wrapped();

    }

    system.output_solution();

    return 0;
}