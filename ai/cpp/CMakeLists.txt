cmake_minimum_required(VERSION 3.10)
project(cpp_training)

set(CMAKE_CXX_STANDARD 17)

add_executable(test lib/test.cpp)
add_executable(omu omu/greedy.cpp)