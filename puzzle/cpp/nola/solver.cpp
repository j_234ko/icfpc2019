#include "lib.cpp"

void all_include_inside(vector<Point> inside_list, vector<vector<int>> &area) {
    int min_x = INF, min_y = INF;
    int max_x = -1, max_y = -1;
    for (auto inside: inside_list) {
        min_x = min(min_x, inside.x);
        min_y = min(min_y, inside.y);
        max_x = max(max_x, inside.x);
        max_y = max(max_y, inside.y);
    }

    REP(x, min_x, max_x + 1) {
        REP(y, min_y, max_y + 1) {
            area[x][y] = 1;
        }
    }
}

Map calc_init_map(PuzzleParameter parameter) {
    Map map(parameter);

//    // inside point をすべて含む長方形をつくる
//    all_include_inside(parameter.i_seq, map.area);

    return map;
}

// 移動可にする
void remove_obstacle(int x, int y, vector<vector<int>> &area, vector<Point> &operations) {
    int dir = randxor() % 4;

    while (inside(x, y, area) && !area[x][y]) {
        area[x][y] = 1;
        operations.emplace_back(Point(x, y));
        x += dx[dir];
        y += dy[dir];
    }
}

// 移動不可にする
void set_obstacle(int x, int y, vector<vector<int>> &area, vector<Point> &operations, PuzzleParameter parameter) {

    // 端に近い方に向かって最短距離でエリアを消していく
    vector<pair<int, int>> v_dir;
    v_dir.emplace_back(make_pair(x + 1, LEFT));
    v_dir.emplace_back(make_pair(area[0].size() - y - 1, UP));
    v_dir.emplace_back(make_pair(y + 1, DOWN));
    v_dir.emplace_back(make_pair(area.size() - x - 1, RIGHT));
    sort(v_dir.begin(), v_dir.end());

    queue<pair<Point, vector<Point>>> q;
    q.push(make_pair(Point(x, y), vector<Point>(1, Point(x, y))));

    set<Point> visited;

    while (q.size()) {
        auto fr = q.front();
        q.pop();

        if (visited.find(fr.first) != visited.end()) {
            continue;
        }

        visited.insert(fr.first);

//        cerr << fr.first.x << " " << fr.first.y << endl;

        rep(dir, 4) {
            int tx = fr.first.x + dx[dir];
            int ty = fr.first.y + dy[dir];
            if (!inside(tx, ty, area) || !area[tx][ty]) {
                operations = fr.second;
                for (auto p: fr.second) {
                    area[p.x][p.y] = 0;
                }
                return;
            }
        }

        for (auto x: v_dir) {
            int dir = x.second;
            int tx = fr.first.x + dx[dir];
            int ty = fr.first.y + dy[dir];
            if (inside(tx, ty, area) && parameter.find_inside(Point(tx, ty))) {
                continue;
            }
            fr.second.emplace_back(Point(tx, ty));
            q.push(make_pair(Point(tx, ty), fr.second));
            fr.second.pop_back();
        }
    }
}

// 操作の復元
void restore(vector<vector<int>> &area, vector<Point> &operations) {
    for (auto p: operations) {
        area[p.x][p.y] ^= 1;
    }
}

void increase_vertices(vector<vector<int>> &area, PuzzleParameter &parameter, vector<Point> &operations) {
    assert(parameter.vertices_more());

    while (true) {
        int x = randxor() % area.size();
        int y = randxor() % area[0].size();
        if (parameter.vertices_more() && !area[x][y]) {
            continue;
        }
        if (parameter.vertices_more()) {
            rep(dir, 4) {
                int tx = x + dx[dir];
                int ty = y + dy[dir];
                if (!inside(tx, ty, area) || !area[tx][ty]) {
                    area[x][y] ^= 1;
                    operations.emplace_back(Point(x, y));
                    return;
                }
            }
        }
    }
}

void
increase_vertices_hard(vector<vector<int>> &area, PuzzleParameter &parameter, vector<Point> &operations) {
    assert(parameter.vertices_more_hard());

    vector<Point> candidate;
    rep(x, area.size()) {
        candidate.push_back(Point(x, 0));
        candidate.push_back(Point(x, area[0].size() - 1));
    }

    rep(y, area[0].size()) {
        candidate.push_back(Point(0, y));
        candidate.push_back(Point(area.size() - 1, y));
    }

    Point p;
    int dir = 0;
    p = candidate[randxor() % candidate.size()];
    if (p.x == 0) {
        dir = RIGHT;
    } else if (p.y == 0) {
        dir = UP;
    } else if (p.x == area.size() - 1) {
        dir = LEFT;
    } else {
        dir = DOWN;
    }

    vector<Point> p_vec;
    int change_cnt = 0;
    // 外周から中に向かって1直線に向かうパート
    while (1) {
        if (!inside(p.x, p.y, area)) {
            break;
        }
        p_vec.push_back(p);
        if (area[p.x][p.y] == 1) {
            area[p.x][p.y] = 0;
            change_cnt++;
        }
        if (parameter.calc_value(area) || !parameter.vertices_more_hard()) {
            area[p.x][p.y] = 1;
            p_vec.pop_back();
            change_cnt--;
            break;
        } else {
            p.x += dx[dir];
            p.y += dy[dir];
        }
    }
    if (change_cnt == 0) {
        return;
    }
    vector<Point> p_vec_tmp = p_vec;
    int n_dir = (dir + 1) % 4;
    while (1) {
        Point tp = p_vec[0];
        tp.x += dx[n_dir];
        tp.y += dy[n_dir];
        if (!inside(tp.x, tp.y, area)) {
            break;
        }
        vector<Point> pre_p;
        for (auto &p: p_vec) {
            p.x += dx[n_dir];
            p.y += dy[n_dir];
            if (area[p.x][p.y]) {
                area[p.x][p.y] = 0;
                pre_p.push_back(p);
            }
        }
        if (parameter.calc_value(area) || !parameter.vertices_more_hard()) {
            for (auto p: pre_p) {
                area[p.x][p.y] = 1;
            }
            break;
        }
    }
    p_vec = p_vec_tmp;
    n_dir = (dir + 3) % 4;
    while (1) {
        Point tp = p_vec[0];
        tp.x += dx[n_dir];
        tp.y += dy[n_dir];
        if (!inside(tp.x, tp.y, area)) {
            break;
        }
        vector<Point> pre_p;
        for (auto &p: p_vec) {
            p.x += dx[n_dir];
            p.y += dy[n_dir];
            if (area[p.x][p.y]){
                area[p.x][p.y] = 0;
                pre_p.push_back(p);
            }
        }
        if (parameter.calc_value(area) || !parameter.vertices_more_hard()) {
            for (auto p: pre_p) {
                area[p.x][p.y] = 1;
            }
            break;
        }
    }
}


//int x = randxor() % area.size();
//int y = randxor() % area[0].size();
//REP(xx, x, x + square_size) {
//REP(yy, y, y + square_size) {
//if (
//inside(x, y, area
//) && area[x][y] == 1) {
//area[x][y] ^= 1;
//operations.
//emplace_back(Point(x, y)
//);
//}
//            rep(dir, 4) {
//                int tx = x + dx[dir];
//                int ty = y + dy[dir];
//                if (!inside(tx, ty, area) || !area[tx][ty]) {
//                    area[x][y] ^= 1;
//                    operations.emplace_back(Point(x, y));
//                }
//            }
//}
//}
//}

void decrease_vertices(vector<vector<int>> &area, PuzzleParameter &parameter, vector<Point> &operations) {
    assert(parameter.vertices_fewer());
    while (true) {
        int x = randxor() % area.size();
        int y = randxor() % area[0].size();
        if (parameter.vertices_more() && !area[x][y]) {
            continue;
        }
        if (parameter.vertices_fewer() && area[x][y]) {
            continue;
        }
        if (parameter.vertices_fewer()) {
            rep(dir, 4) {
                int tx = x + dx[dir];
                int ty = y + dy[dir];
                if (!inside(tx, ty, area) || !area[tx][ty]) {
                    area[x][y] ^= 1;
                    operations.emplace_back(Point(x, y));
                    return;
                }
            }
        }
    }
}

//void resize(vector<vector<int>> &area, PuzzleParameter &parameter, vector<Point> &operations) {
//    int dir = randxor() % 4;
//
//    if (dir == LEFT) {
//        // left
//        rep(y, area[0].size()) {
//            if (area[parameter.min_x][y]) {
//                area[parameter.min_x][y] = 0;
//                operations.emplace_back(Point(parameter.min_x, y));
//            }
//        }
//    }
//    else if(dir == DOWN) {
//        rep(x, area.size()) {
//            if (area[x][parameter.min_y]) {
//                area[x][parameter.min_y] = 0;
//                operations.emplace_back(Point(x, parameter.min_y));
//            }
//        }
//    }
//    else if (dir == RIGHT) {
//        rep(y, area[0].size()) {
//            if (area[parameter.max_x-1][y]) {
//                area[parameter.max_x-1][y] = 0;
//                operations.emplace_back(Point(parameter.max_x-1, y));
//            }
//        }
//    }
//    else if(dir == UP) {
//        rep(x, area.size()) {
//            if (area[x][parameter.max_y-1]) {
//                area[x][parameter.max_y-1] = 0;
//                operations.emplace_back(Point(x, parameter.max_y-1));
//            }
//        }
//    }
//}

Map solve(PuzzleParameter parameter) {

    auto map = calc_init_map(parameter);

    int square_size = 1;
    int failed_square_count = 0;

    int menseki = INF;
    Map ans_map(parameter);

    int cnt = 0;

    int total_cnt = 0;

    while (menseki == INF || cnt++ < 400) {

        bool is_vertex_increase = false;
        total_cnt++;

        int current_value = parameter.calc_value(map.area);

        // outside をどうにかする
        vector<Point> target_candidates;
        for (auto p: parameter.o_seq) {
            if (map.area[p.x][p.y]) {
                target_candidates.emplace_back(p);
            }
        }

        vector<Point> operations;
        if (target_candidates.size()) {
            auto target = target_candidates[randxor() % target_candidates.size()];
            set_obstacle(target.x, target.y, map.area, operations, parameter);
        } else {
            // 頂点数の調整
            if (parameter.vertices_more()) {
                increase_vertices(map.area, parameter, operations);
            } else if (parameter.vertices_fewer()) {
                decrease_vertices(map.area, parameter, operations);
            }
            else if(parameter.vertices_more_hard()) {
                increase_vertices_hard(map.area, parameter, operations);
            }
            else {
                return ans_map;
            }
        }

        int next_value = parameter.calc_value(map.area);

        if (next_value == 0) {
            if (menseki > parameter.ok_list.size()) {
                ans_map = map;
                menseki = parameter.ok_list.size();
                if (enabled_debug) {
                    dump(menseki);
                }
            }
        } else {
        }

        if (next_value < current_value) {
        } else {
            restore(map.area, operations);
        }

        if (total_cnt % 300 == 0) map.debug();
    }

    if (enabled_debug) {
        dum(cnt)
        dump(total_cnt)
    }
    return ans_map;
}

int main(int argc, char *argv[]) {
    PuzzleParameter parameter = puzzle_init(argc, argv);

//    if (enabled_debug) {
//        // 実際はCの字型
//        vector<vector<int>> area{
//                {0, 0, 0, 0, 0},
//                {0, 0, 0, 0, 0},
//                {1, 1, 1, 1, 1},
//                {1, 0, 0, 0, 1},
//                {1, 1, 0, 1, 1},
//        };
//
//        for (int y = area[0].size() - 1; y >= 0; y--) {
//            for (int x = 0; x < area.size(); x++) {
//                cerr << area[x][y] << " ";
//            }
//            cerr << endl;
//        }
//        cerr << endl;
//
//        assert(parameter.all_check(area));
//
//        auto map = Map(parameter);
//        map.area = area;
////        map.output_solution("");
//        return 0;
//    }

    auto map = solve(parameter);

    map.output_solution();

    return 0;
}