#ifndef LIB_CPP
#define LIB_CPP


#include "define.cpp"
#include "parameter.cpp"

PuzzleParameter puzzle_read_conf() {

    auto f = ifstream(puzzle_input_filepath.c_str(), ios::in | ios::binary);

    if (!f) {
        cerr << "ファイルが開けません" << endl;
        exit(1);
    }

    string input;
    f >> input;

    return PuzzleParameter(input);
}

void puzzle_write_desc(string output) {

    auto f = ofstream(puzzle_output_filepath.c_str());

    if (!f) {
        cerr << "ファイルが開けません" << endl;
        exit(1);
    }
    f << output;
}

void dump_vec2(const vector<vector<int>> &v2) {

    int w = v2.size();
    int h = v2[0].size();

    dum(w)
    dump(h)

    for (int y = v2[0].size() - 1; y >= 0; y--) {
        for (int x = 0; x < v2.size(); x++) {
            cerr << v2[x][y];
        }
        cerr << endl;
    }
    cerr << endl;

}

Point max_pos(vector<Point> &vp) {

    int max_y = 0;
    int max_x = 0;

    for (auto p: vp) {
        max_x = max(max_x, p.x);
        max_y = max(max_y, p.y);
    }

    return Point(max_x, max_y);
}

Point min_pos(vector<Point> &vp) {

    int min_y = INF;
    int min_x = INF;

    for (auto p: vp) {
        min_x = min(min_x, p.x);
        min_y = min(min_y, p.y);
    }

    return Point(min_x, min_y);
}

bool calc_area_dfs(Point pos, vector<vector<int>> &area, vector<vector<int>> &visited) {

    if (area[pos.x][pos.y] == 1) {
        return true;
    }

    if (pos.x == 0 || pos.y == 0 || pos.x == area.size() - 1 || pos.y == area[0].size() - 1) {
        return false;
    }

    if (visited[pos.x][pos.y]) {
        return true;
    }

    visited[pos.x][pos.y] = 1;

    bool ok = true;

    rep(i, 4) {
        int tx = pos.x + dx[i];
        int ty = pos.y + dy[i];

        ok = ok && calc_area_dfs(Point(tx, ty), area, visited);
    }

    return ok;
}

void fill_area(vector<Point> &v, vector<vector<int>> &area) {

    vector<vector<int>> visited(area.size(), vector<int>(area[0].size(), 0));

    queue<Point> que;

    for (int x = 0; x < area.size(); x++) {
        for (int y = 0; y < area[0].size(); y += area[0].size() - 1) {
            if (area[x][y] == 0) {
                que.push(Point(x, y));
                visited[x][y] = 1;
            }
        }
    }
    for (int y = 0; y < area[0].size(); y++) {
        for (int x = 0; x < area.size(); x += area.size() - 1) {
            if (area[x][y] == 0) {
                que.push(Point(x, y));
                visited[x][y] = 1;
            }
        }
    }

    while (!que.empty()) {

        auto p = que.front();

        que.pop();

        rep(i, 4) {
            int tx = p.x + dx[i];
            int ty = p.y + dy[i];

            if (!check(tx, ty, area.size(), area[0].size())) {
                continue;
            }

            if (area[tx][ty] == 1) {
                continue;
            }

            if (!visited[tx][ty]) {
                visited[tx][ty] = 1;
                que.push(Point(tx, ty));
            }
        }
    }

    rep(x, area.size()) {
        rep(y, area[0].size()) {
            if (visited[x][y] == 1) {
                area[x][y] = 0;
            } else {
                area[x][y] = 1;
            }
        }
    }

}

/**
 * 頂点の集合から、エリアを計算する
 * 1 がエリア内, 0 がエリア外
 */
vector<vector<int>> calc_area(int W, int H, vector<Point> &v) {

    Point mp = max_pos(v);

    assert(mp.x > 0);
    assert(mp.y > 0);

    vector<vector<int>> area(W, vector<int>(H, 0));


    int n = v.size();

    rep(f, n + 1) {

        int i = f % n;
        int j = (i + 1) % n;
        int k = (j + 1) % n;

        assert (v[i].x == v[j].x || v[i].y == v[j].y);
        assert (v[j].x == v[k].x || v[j].y == v[k].y);

        if (v[i].x == v[j].x) {

            int x = v[i].x;
            int min_y = min(v[i].y, v[j].y);
            int max_y = max(v[i].y, v[j].y);

            if (v[i].y < v[j].y) {
                x--;
            }

            for (int y = min_y; y < max_y; y++) {
                area[x][y] = 1;
            }

        } else if (v[i].y == v[j].y) {

            int y = v[i].y;
            int min_x = min(v[i].x, v[j].x);
            int max_x = max(v[i].x, v[j].x);

            if (v[i].x > v[j].x) {
                y--;
            }

            for (int x = min_x; x < max_x; x++) {
                area[x][y] = 1;
            }
        }
    }

    fill_area(v, area);

    return area;

}

class Map {

    vector<Point> ok_list = {Point(-1, -1)};

    string vertices_str() {
        auto v = Converter::convert_area_2_vertex_vec(area);
        string ret = "";
        for (auto p: v) {
            if (ret.size()) ret += ",";
            ret += p.to_string();
        }
        return ret;
    }

    void bfs(Point start, vector<vector<int>> &visited, vector<Point> &order) {
        queue<Point> q;
        q.push(start);

        while(q.size()) {
            auto p = q.front();
            q.pop();

            if (!inside(p.x, p.y, area) || visited[p.x][p.y] || !area[p.x][p.y]) {
                continue;
            }
            visited[p.x][p.y] = 1;
            order.push_back(p);
            rep(i, 4) {
                auto next = p;
                next.x += dx[i];
                next.y += dy[i];
                q.push(next);
            }
        }
    }

public:
    /*
     * 移動可能マスかどうかを格納する
     * 1: 移動可能
     * 0: 移動不可
     */
    vector<vector<int>> area;
    PuzzleParameter parameter;

    Map(PuzzleParameter _parameter) : parameter(_parameter) {
        area = vector<vector<int>>(_parameter.t_size, vector<int>(_parameter.t_size, 1));
    }

    bool check_solution() {
        return parameter.all_check(area);
    }

//    int calc_value() {
//        return parameter.calc_value(area);
//    }

    void output_solution() {
        string ret = vertices_str() + "#";

        vector<vector<int>> visited(area.size(), vector<int>(area[0].size(), 0));
        vector<Point> order;
        rep(x, area.size()) {
            rep(y, area[0].size()) {
                if (area[x][y]) {
                    bfs(Point(x, y), visited, order);
                    break;
                }
            }
            if (order.size()) {
                break;
            }
        }


        int ind = 0;
        ret += order[ind++].to_string() + "##";

        vector<string> booster_v;
        rep(i, parameter.c_num) booster_v.emplace_back("C" + order[ind++].to_string());
        rep(i, parameter.x_num) booster_v.emplace_back("X" + order[ind++].to_string());
        rep(i, parameter.m_num) booster_v.emplace_back("B" + order[ind++].to_string());
        rep(i, parameter.f_num) booster_v.emplace_back("F" + order[ind++].to_string());
        rep(i, parameter.d_num) booster_v.emplace_back("L" + order[ind++].to_string());
        rep(i, parameter.r_num) booster_v.emplace_back("R" + order[ind++].to_string());

        string booster_str = "";
        for (auto s: booster_v) {
            if (booster_str.size()) booster_str += ";";
            booster_str += s;
        }

        ret += booster_str;
        puzzle_write_desc(ret);
    }

    void debug() {
        parameter.debug(area);

        if (enabled_debug) {
            for (int y = area[0].size() - 1; y >= 0; y--) {
                for (int x = 0; x < area.size(); x++) {
                    cerr << area[x][y] << " ";
                }
                cerr << endl;
            }
            cerr << endl;
        }
    }
};

PuzzleParameter puzzle_init(int argc, char *argv[]) {

    if (argc >= 2) {
        puzzle_input_filepath = string(argv[1]);
    }
    if (argc >= 3) {
        puzzle_output_filepath = string(argv[2]);
    }
    if (argc >= 4) {
        string debug_str = string(argv[3]);
        if (debug_str == "--no-debug") {
            enabled_debug = false;
//            return PuzzleParameter("1,1,5,11,13,0,0,2,8,0,0#(4,3),(3,0),(2,1),(2,4)#(0,4),(1,3),(3,2),(4,2)");
        }
    }

    if (enabled_debug) {
        cerr << R"({ "puzzle_name": ")" + puzzle_input_filepath + "\" }" << endl;
        cerr << R"({ "map_name": ")" + puzzle_output_filepath + "\" }" << endl;
    }

    return puzzle_read_conf();
}


#endif

