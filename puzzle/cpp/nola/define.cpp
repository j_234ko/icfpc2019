
#ifndef DEFINE_CPP
#define DEFINE_CPP

#include <iostream>
#include <vector>
#include <map>
#include <unordered_map>
#include <set>
#include <unordered_set>
#include <string>
#include <cstring>
#include <sstream>
#include <algorithm>
#include <functional>
#include <queue>
#include <stack>
#include <cmath>
#include <iomanip>
#include <list>
#include <tuple>
#include <bitset>
#include <ciso646>
#include <cassert>
#include <complex>
#include <random>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <thread>
#include <chrono>
#include <ratio>
#include <bitset>
#include <fstream>

#include <cstdio>
#include <ctime>
#include <cstdio>
#include <sys/types.h>
#include <sys/timeb.h>

#define rep(i, n)    for(int (i) = 0; (i) < (n); (i)++)
#define REP(i, a, n)    for(int (i) = (a); (i) < (n); (i)++)
#define each(i, n)    for(auto &i : n)
#define clr(a)        memset((a), 0 ,sizeof(a))
#define mclr(a)        memset((a), -1 ,sizeof(a))
#define all(a)        (a).begin(),(a).end()
#define dum(val)     cerr << #val " = " << val << " ";
#define dump(val)    cerr << #val " = " << val << endl;
#define abs(a)       ((a) > 0 ? (a) : -(a))

using namespace std;

bool enabled_debug = true;

template<class T>
string tostr(T x) {
    stringstream o;
    o << x;
    return o.str();
}


int toint(const string &s) {
    stringstream ss;
    ss << s;
    int v;
    ss >> v;
    return v;
}

vector<string> split(string s, const string &t) {
    vector<string> ret;
    ret.emplace_back();

    int ts = t.size();
    rep(i, s.size()) {
        if (s.substr(i, ts) == t) {
            i += ts - 1;
            ret.emplace_back();
        } else {
            ret[ret.size() - 1] += s[i];
        }
    }

    if (ret[ret.size() - 1].empty()) ret.pop_back();
    return ret;
}

int reverse_move_dir(int move_dir) {
    return (move_dir + 2) % 4;
}

bool check(int x, int y, int W, int H) {
    return x >= 0 && y >= 0 && x < W && y < H;
}

const int dx[8] = {+1, 0, -1, 0, +1, -1, -1, +1};
const int dy[8] = {0, -1, 0, +1, +1, -1, +1, -1};

const int RIGHT = 0;
const int DOWN = 1;
const int LEFT = 2;
const int UP = 3;
const int RIGHT_UP = 4;
const int LEFT_DOWN = 5;
const int LEFT_UP = 6;
const int RIGHT_DOWN = 7;


struct Point {
    int x, y;

    Point() { x = -1, y = -1; }

    Point(int x, int y)
            : x(x), y(y) {}

    bool operator<(const Point &other) const {
        return x < other.x || (x == other.x && y < other.y);
    }

    bool operator==(const Point &other) const {
        return (x == other.x && y == other.y);
    }

    string to_string() {
        return "(" + std::to_string(x) + "," + std::to_string(y) + ")";
    }
};

vector<Point> WRAP_RANGE_LIST[4];

void init_define() {
    WRAP_RANGE_LIST[RIGHT].emplace_back(0, 0);
    WRAP_RANGE_LIST[RIGHT].emplace_back(+1, +1);
    WRAP_RANGE_LIST[RIGHT].emplace_back(+1, 0);
    WRAP_RANGE_LIST[RIGHT].emplace_back(+1, -1);

    WRAP_RANGE_LIST[UP].emplace_back(0, 0);
    WRAP_RANGE_LIST[UP].emplace_back(-1, +1);
    WRAP_RANGE_LIST[UP].emplace_back(0, +1);
    WRAP_RANGE_LIST[UP].emplace_back(+1, +1);

    WRAP_RANGE_LIST[DOWN].emplace_back(0, 0);
    WRAP_RANGE_LIST[DOWN].emplace_back(-1, -1);
    WRAP_RANGE_LIST[DOWN].emplace_back(0, -1);
    WRAP_RANGE_LIST[DOWN].emplace_back(1, -1);

    WRAP_RANGE_LIST[LEFT].emplace_back(0, 0);
    WRAP_RANGE_LIST[LEFT].emplace_back(-1, +1);
    WRAP_RANGE_LIST[LEFT].emplace_back(-1, 0);
    WRAP_RANGE_LIST[LEFT].emplace_back(-1, -1);
}

const int INF = 1e9;

bool inside(int x, int y, vector<vector<int>> &area) {
    return 0 <= x && x < area.size() && 0 <= y && y < area[0].size();
}

namespace Converter {

    Point read_point(string s) {

        if (s[0] == '(') {
            s = s.substr(1);
        }

        if (s[s.size() - 1] == ')') {
            s = s.substr(0, s.size() - 1);
        }

        auto v = split(s, ",");

        return Point(toint(v[0]), toint(v[1]));

    }

    vector<Point> convert_str_vec_2_point_vec(const vector<string> &v) {
        vector<Point> res;

        for (const auto &t: v) {
            res.push_back(read_point(t));
        }

        return res;
    }

    bool ok(int x, int y, vector<vector<int>> &area) {
        return inside(x, y, area) && area[x][y];
    }

    tuple<int, int> find_upper_left_corner(vector<vector<int>> &area) {
        int W = area.size();
        int H = area[0].size();

        rep(x, W) {
            rep(y, H) {
                if (ok(x, y, area) && !ok(x - 1, y, area) && !ok(x, y + 1, area)) {
                    return make_tuple(x, y);
                }
            }
        }
        assert(0);
    }

    vector<Point> convert_area_2_vertex_vec(vector<vector<int>> &area) {
        int dir = DOWN, x, y;
        tie(x, y) = find_upper_left_corner(area);

        // 初期状態
        int s_dir = dir, s_x = x, s_y = y;

        vector<Point> ret;

        // 左手法でなぞる
        do {
            if (dir == DOWN) {
                if (!ok(x, y - 1, area)) {
                    dir = RIGHT;
                    ret.emplace_back(Point(x, y));
                } else if (ok(x - 1, y - 1, area)) {
                    dir = LEFT;
                    ret.emplace_back(Point(x, y));
                    x--;
                    y--;
                } else {
                    y--;
                }
            } else if (dir == RIGHT) {
                if (!ok(x + 1, y, area)) {
                    dir = UP;
                    ret.emplace_back(Point(x + 1, y));
                } else if (ok(x + 1, y - 1, area)) {
                    dir = DOWN;
                    ret.emplace_back(Point(x + 1, y));
                    x++;
                    y--;
                } else {
                    x++;
                }
            } else if (dir == UP) {
                if (!ok(x, y + 1, area)) {
                    dir = LEFT;
                    ret.emplace_back(Point(x + 1, y + 1));
                } else if (ok(x + 1, y + 1, area)) {
                    dir = RIGHT;
                    ret.emplace_back(Point(x + 1, y + 1));
                    x++;
                    y++;
                } else {
                    y++;
                }
            } else if (dir == LEFT) {
                if (!ok(x - 1, y, area)) {
                    dir = DOWN;
                    ret.emplace_back(Point(x, y + 1));
                } else if (ok(x - 1, y + 1, area)) {
                    dir = UP;
                    ret.emplace_back(Point(x, y + 1));
                    x--;
                    y++;
                } else {
                    x--;
                }
            }
        } while (!(dir == s_dir && x == s_x && y == s_y));

        return ret;
    }
}

unsigned int randxor()
{
    static unsigned int x=123456789,y=362436069,z=521288629,w=88675123;
    unsigned int t;
    t=(x^(x<<11));x=y;y=z;z=w; return( w=(w^(w>>19))^(t^(t>>8)) );
}

string puzzle_input_filepath = "../../../puzzle-task/puzzle-001.cond";
string puzzle_output_filepath = "../../../puzzle-task/puzzle-001.desc";

#endif
