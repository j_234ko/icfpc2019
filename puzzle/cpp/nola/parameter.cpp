#ifndef COMMAND_CPP
#define COMMAND_CPP

#include "./define.cpp"

class PuzzleParameter {

    bool is_connected;
    vector<Point> vertex_list;

    void dfs(Point start, vector<vector<int>> &area, vector<vector<int>> &visited) {
        stack<Point> st;
        st.push(start);
        while(st.size()) {
            auto pos = st.top();
            st.pop();
            if (!inside(pos.x, pos.y, area) || !area[pos.x][pos.y] || visited[pos.x][pos.y]) {
                continue;
            }
            visited[pos.x][pos.y] = 1;

            rep(dir, 4) {
                auto next = pos;
                next.x += dx[dir];
                next.y += dy[dir];
                st.push(next);
            }
        }
    }

    void init(vector<vector<int>> &area) {
        ok_list.clear();
        is_connected = true;
        max_x = max_y = 0;
        min_x = min_y = INF;
        rep(x, area.size()) {
            rep(y, area[0].size()) {
                if (!area[x][y]) continue;
                max_x = max(max_x, x + 1);
                max_y = max(max_y, y + 1);
                min_x = min(min_x, x);
                min_y = min(min_y, y);
                ok_list.emplace_back(Point(x, y));
            }
        }
        auto visited = vector<vector<int>>(area.size(), vector<int>(area[0].size()));
        dfs(ok_list[0], area, visited);

        for(auto ok_pos: ok_list) {
            if (!visited[ok_pos.x][ok_pos.y]) {
                is_connected = false;
            }
        }

        vertex_list = Converter::convert_area_2_vertex_vec(area);
    }

public:
    int b_num, e_num, t_size, v_min, v_max, m_num, f_num, d_num, r_num, c_num, x_num;

    vector<Point> i_seq;
    vector<Point> o_seq;
    vector<Point> ok_list;
    int max_x, max_y;
    int min_x, min_y;

    PuzzleParameter(string input) {
        vector<string> vs = split(input, "#");

        auto params = split(vs[0], ",");
        b_num = toint(params[0]);
        e_num = toint(params[1]);
        t_size = toint(params[2]);
        v_min = toint(params[3]);
        v_max = toint(params[4]);
        m_num = toint(params[5]);
        f_num = toint(params[6]);
        d_num = toint(params[7]);
        r_num = toint(params[8]);
        c_num = toint(params[9]);
        x_num = toint(params[10]);

        i_seq = Converter::convert_str_vec_2_point_vec(split(vs[1], "),("));
        o_seq = Converter::convert_str_vec_2_point_vec(split(vs[2], "),("));
        sort(i_seq.begin(), i_seq.end());
        sort(o_seq.begin(), o_seq.end());
    }

    bool find_inside(Point x) {
        return find(i_seq.begin(), i_seq.end(), x) != i_seq.end();
    }

    bool find_outside(Point x) {
        return find(o_seq.begin(), o_seq.end(), x) != o_seq.end();
    }

    int smaller() {
        return max(0, max(max_x, max_y) - t_size);
    }

    int larger() {
        // この計算で正確か怪しい
        return max(0, t_size - (int) floor(0.1 * t_size) - max(max_x - min_x, max_y - min_y));
    }

    int area_larger() {
        return max(0, (int) ceil(0.2 * t_size * t_size) - (int) ok_list.size());
    }

    int vertices_fewer() {
        return max(0, (int) vertex_list.size() - v_max);
    }

    // 制限に対してどれだけ頂点数が少ないかを表す
    int vertices_more() {
        return max(0, v_min - (int) vertex_list.size());
    }

//    int vertices_fewer_hard() {
//        int hard_v_max = v_min + (v_max - v_min) / 3 * 2;
//        return max(0, (int) vertex_list.size() - hard_v_max);
//    }

    int vertices_more_hard() {
        int hard_v_min = v_min + (v_max - v_min) / 3 * 2;
        return max(0, hard_v_min - (int) vertex_list.size());
    }

    int available_num_larger() {
        // 初期位置 + ブースター数
        return max(0, 1 + m_num + f_num + d_num + r_num + c_num + x_num - (int) ok_list.size());
    }

    bool is_connect() {
        return is_connected;
    }

    int not_inside_squares(vector<vector<int>> &area) {
        int cnt = 0;
        for (auto square: i_seq) {
            if (!area[square.x][square.y]) {
                cnt++;
            }
        }
        return cnt;
    }

    int not_outside_squares(vector<vector<int>> &area) {
        int cnt = 0;
        for (auto square: o_seq) {
            if (area[square.x][square.y]) {
                cnt++;
            }
        }
        return cnt;
    }

    // Validかを判定
    // areaが連結かどうか、障害物を含まないかは見てない
    int all_check(vector<vector<int>> &area) {
        return calc_value(area);
    }

    // 評価値を計算
    int calc_value(vector<vector<int>> &area) {
        init(area);

        int ret = 0;
        ret += smaller();
        ret += larger();
        ret += area_larger();
        ret += vertices_fewer();
        ret += vertices_more();
        ret += available_num_larger();
        ret += not_inside_squares(area);
        ret += not_outside_squares(area);
        ret += !is_connect();
        return ret;

        return smaller() + larger() + area_larger() + vertices_fewer() + vertices_more() + available_num_larger() +
               not_inside_squares(area) + not_outside_squares(area);
    }

    void debug(vector<vector<int>> &area) {
        if (enabled_debug) {
            init(area);
            cerr << R"({ "smaller": ")" + to_string(smaller()) + "\" }" << endl;
            cerr << R"({ "larger": ")" + to_string(larger()) + "\" }" << endl;
            cerr << R"({ "area_larger": ")" + to_string(area_larger()) + "\" }" << endl;
            cerr << R"({ "vertices_fewer": ")" + to_string(vertices_fewer()) + "\" }" << endl;
            cerr << R"({ "vertices_more": ")" + to_string(vertices_more()) + "\" }" << endl;
            cerr << R"({ "available_num_larger": ")" + to_string(available_num_larger()) + "\" }" << endl;
            cerr << R"({ "not_inside_squares": ")" + to_string(not_inside_squares(area)) + "\" }" << endl;
            cerr << R"({ "not_outside_squares": ")" + to_string(not_outside_squares(area)) + "\" }" << endl;
            cerr << R"({ "is_connect": ")" + to_string(is_connect()) + "\" }" << endl;

        }
    }
};


#endif