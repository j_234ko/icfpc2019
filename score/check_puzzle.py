from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from collections import OrderedDict
from typing import List, Dict, Tuple, Optional, Any
import time
import glob
import os
from pathlib import Path

current_dir = os.getcwd()

class ScoreCrowlar(object):

    # url = 'http://localhost:8000/checker.html'
    url = f"file://{current_dir}/site_data/puzzle.html"

    def __init__(self):
        from datetime import datetime

        self.start_time: datetime = datetime.now()

    def open_browser(
        self,
        driver_path: str = "/usr/lib/chromium-browser/chromedriver",
        options: List[str] = list(),
    ):
        if getattr(self, "browser", None):
            self.browser.quit()
            raise Exception("Multi browser open !!")
        if not options:
            options = ["--headless"]
        driver_options = Options()
        for opt in options or []:
            driver_options.add_argument(opt)
        self.browser: webdriver = webdriver.Chrome(
            executable_path=driver_path, options=driver_options
        )
        self.browser.get(self.url)

    def close_browser(self):
        if not getattr(self, "browser", None):
            raise Exception("No browser open !!")
        self.browser.quit()

    def select_check_box(self, _id):
        self.browser.find_element_by_id(_id).click()

    def set_file(self, _id, filepath):
        for element in self.browser.find_elements_by_tag_name("input"):
            if element.get_attribute("id") == _id:
                element.send_keys(filepath)
                break

    def submit(self):
        self.browser.find_element_by_id("execute_solution").click()

    def get_result(self):
        return self.browser.find_element_by_id("output").text

    def set_sol_model_file_in(self, filepath):
        self.set_file("submit_solution", filepath)

    def set_task_model_file_in(self, filepath):
        self.set_file("submit_task", filepath)

    def get_score(
        self,
        task_file_path: str,
        sol_file_path: str,
        error_filename=f"{current_dir}/error_files",
    ):
        import traceback

        def _save_screanshot(error_flag=True):
            if error_flag:
                print(f"error: {task_file_path}, show screan shot")
            self.save_screenshot(error_filename, task_file_path)

        current_score = 0
        try:
            try:
                if not task_file_path.endswith("cond"):
                    print(f"file skip ${task_file_path}")
                    return current_score
                if not sol_file_path.endswith("desc"):
                    print(f"file skip ${sol_file_path}")
                    return current_score
                self.set_task_model_file_in(task_file_path)
                self.set_sol_model_file_in(sol_file_path)
                # アップロードを待つ
                time.sleep(2)

                self.submit()
                count = 0
                sleep_time = 0.1
                # 成功するまで待ち見続ける必要がある
                while count <= 20:
                    time.sleep(sleep_time)
                    result = self.get_result()
                    if "Success" in result:
                        _save_screanshot(False)
                        break
                    if "Failed" in result:
                        _save_screanshot()
                        return -1
                    if "Cannot" in result:
                        _save_screanshot()
                        return -1
                    count += 1
                    sleep_time = min(5, sleep_time * 1.25)
            except Exception as e:
                print(traceback.format_exc())
                _save_screanshot()
        except Exception as e:
            print(traceback.format_exc())
            _save_screanshot()
        return current_score

    def save_screenshot(self, filename, task_file_path):
        task_filename = task_file_path.split("/")[-1]
        self.browser.save_screenshot(f"{filename}/check_{task_filename}.png")

if __name__ == "__main__":

    m = {
        "puzzle_16.cond": "puzzle_sol_16.desc",
        "puzzle_17.cond": "puzzle_sol_17.desc",
        "puzzle_18.cond": "puzzle_sol_18.desc",
        "puzzle_19.cond": "puzzle_sol_19.desc",
        "puzzle_21.cond": "puzzle_sol_21.desc",
        "puzzle_22.cond": "puzzle_sol_22.desc",
        "puzzle_23.cond": "puzzle_sol_23.desc",
        "puzzle_24.cond": "puzzle_sol_24.desc",
        "puzzle_25.cond": "puzzle_sol_25.desc",
        "puzzle_26.cond": "puzzle_sol_26.desc",
        "puzzle_27.cond": "puzzle_sol_27.desc",
        "puzzle_28.cond": "puzzle_sol_28.desc",
        "puzzle_29.cond": "puzzle_sol_29.desc",
        "puzzle_30.cond": "puzzle_sol_30.desc",
        "puzzle_31.cond": "puzzle_sol_31.desc",
        "puzzle_32.cond": "puzzle_sol_32.desc",        
    }

    t = str(Path(os.getcwd()).parent / "puzzle-task")
    try:
        sc = ScoreCrowlar()
        sc.open_browser()
        for task_file_path in glob.glob(f"{t}/*"):
            task_file_name = Path(task_file_path).name
            sol_file_name = m.get(task_file_name)
            if not sol_file_name:
                continue
            sol_file_path = str(Path(os.getcwd()).parent / "puzzle-sol" / sol_file_name)
            score = sc.get_score(task_file_path, sol_file_path)
            if score != 0:
                print("error")
                print("task_file_path:%s, score:%s" % (task_file_path, score))
    except Exception as e:
        import traceback

        print(traceback.format_exc())
        raise e
    finally:
        sc.close_browser()
