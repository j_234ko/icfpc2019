from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time

class Visualizer:
    def __init__(self, executable_path):
        self.driver = webdriver.Chrome(executable_path=executable_path)
        self.driver.get('https://icfpcontest2019.github.io/solution_visualiser/')

    def get_taskinput_elm(self):
        return self.driver.find_element_by_id('submit_task')

    def get_solution_input_elm(self):
        return self.driver.find_element_by_id('submit_solution')

    def prepare_to_run_button(self):
        return self.driver.find_element_by_id('execute_solution')

    def visualize_area(self):
        return self.driver.find_element_by_id('canvas')

    def body(self):
        return self.driver.find_element_by_tag_name('body')

    def visualize(self, task_path, sol_path):
        self.get_taskinput_elm().send_keys(task_path)
        self.get_solution_input_elm().send_keys(sol_path)
        time.sleep(1)

        self.driver.execute_script("window.scrollTo(0, 130);")

        # prepare to run
        self.body().send_keys('r')

        # start execution
        time.sleep(3)
        self.body().send_keys('s')
        time.sleep(300)


def main():
    import sys
    exepath = sys.argv[1]
    task = sys.argv[2]
    sol = sys.argv[3]
    visualizer = Visualizer(exepath)
    print(visualizer.visualize(task, sol))

if __name__ == '__main__':
    main()

