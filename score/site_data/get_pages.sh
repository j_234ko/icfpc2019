#!/bin/sh
HTML_FILE_NAME='checker.html'
VISUAL_HTML_FILE_NAME='visualiser.html'
PUZZLE_HTML_FILE_NAME='puzzle.html'
FILES_DIR_NAME='html_files'
CSS_FILE_NAME='main.css'
JS_FILE_NAME='lambda.js'

mkdir -p $FILES_DIR_NAME
curl https://icfpcontest2019.github.io/solution_checker/ > $HTML_FILE_NAME
curl https://icfpcontest2019.github.io/assets/main.css > $FILES_DIR_NAME/$CSS_FILE_NAME
curl https://icfpcontest2019.github.io/static/lambda.js > $FILES_DIR_NAME/$JS_FILE_NAME
sed -e 's|/assets/main.css|./html_files/main.css|g' -i $HTML_FILE_NAME
sed -e 's|/static/lambda.js|./html_files/lambda.js|g' -i $HTML_FILE_NAME

curl https://icfpcontest2019.github.io/solution_visualiser/ > $VISUAL_HTML_FILE_NAME
sed -e 's|/assets/main.css|./html_files/main.css|g' -i $VISUAL_HTML_FILE_NAME
sed -e 's|/static/lambda.js|./html_files/lambda.js|g' -i $VISUAL_HTML_FILE_NAME

curl https://icfpcontest2019.github.io/puzzle_checker/ > $PUZZLE_HTML_FILE_NAME
sed -e 's|/assets/main.css|./html_files/main.css|g' -i $PUZZLE_HTML_FILE_NAME
sed -e 's|/static/lambda.js|./html_files/lambda.js|g' -i $PUZZLE_HTML_FILE_NAME


