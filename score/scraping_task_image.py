from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from collections import OrderedDict
from typing import List, Dict, Tuple, Optional, Any
import time
import glob
import os
from pathlib import Path

try:
    from tqdm import tqdm
except:
    pass

current_dir = os.getcwd()


class ScoreCrowlar(object):

    # url = 'http://localhost:8000/checker.html'
    url = f"file://{current_dir}/site_data/visualiser.html"

    def __init__(self):
        from datetime import datetime

        self.start_time: datetime = datetime.now()

    def open_browser(
        self,
        driver_path: str = "/usr/lib/chromium-browser/chromedriver",
        options: List[str] = list(),
    ):
        if getattr(self, "browser", None):
            self.browser.quit()
            raise Exception("Multi browser open !!")
        if not options:
            options = ["--headless", "--disable-gpu"]
        driver_options = Options()
        for opt in options or []:
            driver_options.add_argument(opt)
        self.browser: webdriver = webdriver.Chrome(
            executable_path=driver_path, options=driver_options
        )
        self.browser.get(self.url)
        self.browser.maximize_window()
        self.browser.execute_script("window.scrollTo(0, 130);")

    def close_browser(self):
        if not getattr(self, "browser", None):
            raise Exception("No browser open !!")
        self.browser.quit()

    def select_check_box(self, _id):
        self.browser.find_element_by_id(_id).click()

    def set_file(self, _id, filepath):
        for element in self.browser.find_elements_by_tag_name("input"):
            if element.get_attribute("id") == _id:
                element.send_keys(filepath)
                break

    def submit(self):
        self.browser.find_element_by_id("execute_solution").click()

    def get_result(self):
        return self.browser.find_element_by_id("output").text

    def set_sol_model_file_in(self, filepath):
        self.set_file("submit_solution", filepath)

    def set_task_model_file_in(self, filepath):
        self.set_file("submit_task", filepath)

    def screan_shot(
        self,
        task_file_path: str,
        screan_shot_path: Path,
    ) -> int:
        import traceback

        def _save_screanshot() -> None:
            file_name: str = Path(task_file_path).name
            self.save_screenshot(file_name, screan_shot_path)

        current_score = 0
        try:
            if not task_file_path.endswith("desc"):
                # print(f"file skip ${task_file_path}")
                return -1
            self.set_task_model_file_in(task_file_path)
            # アップロードを待つ
            time.sleep(1)
            _save_screanshot()
        except Exception as e:
            print(traceback.format_exc())
            _save_screanshot()
            return 1
        return 0

    def save_screenshot(self, file_name, screan_shot_path):
        self.browser.save_screenshot(f"{screan_shot_path}/{file_name}.png")

if __name__ == "__main__":
    cdir: Path = Path(os.getcwd()).parent
    path_map = ["part-1-initial", "part-2-teleports", "part-3-clones"]
    task_root_path = "tasks"
    paths: List[str] = []
    target_paths = []
    for pm in path_map:
        p = cdir / task_root_path / pm
        target_paths.append(str(p))
    screan_shot_path = cdir / 'task_screan_shot'

    def _driver_path() -> Path:
        return Path(os.getcwd()) / "driver" / "chromedriver"

    try:
        sc = ScoreCrowlar()
        sc.open_browser(driver_path=str(_driver_path()))
        for _t in target_paths:
            for task_file_path in tqdm(glob.glob(f"{_t}/*")):
                if sc.screan_shot(task_file_path, screan_shot_path) > 0:
                    file_name: str = Path(task_file_path).name
                    print(f"failed: {file_name}")
    except Exception as e:
        import traceback

        print(traceback.format_exc())
        raise e
    finally:
        sc.close_browser()
