# やれること

ローカルに落としてきたCheckerが走る

# 下準備

* `../sol/` `../tasks/part-[1|2|3]-hogehoge/` 配下に諸々を置く
    * `proc-XXX.[sol|desc]` 形式のハズ
* `cd site_data; ./get_pages.sh; cd ..` を叩いてサイトをゲット
* ~~`python start_server.py` でローカルホストからアクセスできるようにする~~
    * `file://` による直アクセスで不要
* chromiumを動かすための諸々をゲット（入れてある場合はカット）

```sh
sudo apt update && apt upgrade -y
sudo apt install chromium-browser
sudo apt install chromium-chromedriver
sudo apt install python3-selenium
```

* `pipenv install` する
* `pipenv run checker` する

色々冗長なので改善案を募集

# テストデータ

* prob-001.sol: success パターン
* prob-002.sol: failed パターン

# 参考

* https://qiita.com/tabimoba/items/4ea3404416142187e645