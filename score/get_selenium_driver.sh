#!/bin/sh

set -eu

mkdir -p tmp/
# mac
curl https://chromedriver.storage.googleapis.com/75.0.3770.90/chromedriver_mac64.zip -o tmp/driver.zip
# linux
# curl https://chromedriver.storage.googleapis.com/75.0.3770.90/chromedriver_linux64.zip -o tmp/driver.zip
cd tmp
unzip driver.zip
cd ..
mkdir -p driver/
cp tmp/chromedriver driver/
rm -rdf tmp
