import glob
from pathlib import Path
from typing import List, Dict, Tuple, Optional, Any

from get_submit_score import ScoreCrowlar


import os

current_dir: Path = Path(os.getcwd()).parent


def get_target_dirs(mode: str, target_root_path: Path) -> List[str]:
    path_map = {"a": "part-1-initial", "b": "part-2-teleport", "c": "part-3-clones"}
    task_root_path = "tasks"
    paths: List[str] = []
    if mode == "f":
        target = ["a", "b", "c"]
    else:
        target = [mode]
    target_paths = []
    for t in target:
        m = path_map[t]
        p = current_dir / task_root_path / m
        target_paths.append(str(p))
    return target_paths


def calc_folder_score(
    sc: ScoreCrowlar, task_dir_path: str, user: str = ""
) -> Dict[Any, Dict[str, Any]]:
    score_map: Dict[Any, Dict[str, Any]] = {}
    for task_file_path in sorted(glob.glob(f"{task_dir_path}/*")):
        t = calc_file_score(sc, task_file_path, user)
        if t:
            score_map[t[0]] = {
                "user": user or "Muimi",
                "sol_filepath": t[1],
                "score": t[2],
            }
    return score_map


def calc_file_score(sc: ScoreCrowlar, task_file_path: str, user: str = "") -> Tuple:
    if not task_file_path.endswith("desc"):
        print(f"file skip ${task_file_path}")
        return tuple()
    sol_root_path = "sol"

    task_file_name = Path(task_file_path).name
    sol_file_name = task_file_name.replace("desc", "sol")
    sol_file_path = str(
        Path(
            current_dir
            / (sol_root_path if not user else (Path(sol_root_path) / user))
            / sol_file_name
        )
    )

    score = sc.get_score(task_file_path, sol_file_path)
    # print('task_file_path:%s, score:%s' % (task_file_path, score))
    return (task_file_name, sol_file_path, score)


def update_highest(org: dict, newer: dict) -> None:
    """
    expect:

    [
        {
            "probname": {
                'user': str,
                "sol_filepath": str,
                "score": int
            }
        },
        {
            ...
        },
        ...
    ]
    """

    for n in newer:
        if n in org:
            t = org[n]
            if t["score"] < newer[n]["score"]:
                org[n] = newer[n]
        else:
            org[n] = newer[n]


def get_highest_map(mode: str, user: str) -> Dict[Any, Dict[str, Any]]:
    current_map: Dict[Any, Dict[str, Any]] = {}
    try:
        sc = ScoreCrowlar()
        sc.open_browser()

        for task_dir_path in get_target_dirs(mode, current_dir):
            current_score = calc_folder_score(sc, task_dir_path, user)
            current_map.update(current_score)
    except Exception as e:
        import traceback

        print(traceback.format_exc())
        raise e
    finally:
        sc.close_browser()
    return current_map


def entry_user() -> list:
    ret: List[str] = []
    for i in range(2):
        user = input("user ? >> ").strip()
        ret.append(user)
    return ret


def get_user() -> list:
    # TODO: Username? Program name? like, Manaka Raara, Minami mirei, Hojo Sophie
    return []


def run(mode: str) -> None:
    highest_map: dict = {}
    entry: list = []
    if "i" in mode:
        entry = entry_user()
        mode = mode.replace("i", "")
    else:
        entry = get_user()
    for user in entry:
        d = get_highest_map(mode, user)
        update_highest(highest_map, d)

    print(
        "☆'.･*.･:★'.･*.･:☆'.･*.･:★ ☆'.･*.･:★'.･*.･:☆'.･*.･:★'.･*.･:☆'.･*.･:★'.･*.･:☆'.･*.･:★"
    )
    print("ranking::")
    for k, v in highest_map.items():
        prog = k
        user = v["user"]
        score = v["score"]
        print("prog:%s, user:%s, score:%d" % (prog, user, score))
    print(
        "☆'.･*.･:★'.･*.･:☆'.･*.･:★ ☆'.･*.･:★'.･*.･:☆'.･*.･:★'.･*.･:☆'.･*.･:★'.･*.･:☆'.･*.･:★"
    )


if __name__ == "__main__":
    mode = input("task 1: a, task 2: b, task 3: c, all: f >> ").strip()
    run(mode)
