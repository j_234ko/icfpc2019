from selenium import webdriver
import time

class Checker:
    def __init__(self, executable_path):
        self.driver = webdriver.Chrome(executable_path=executable_path)
        self.driver.get('https://icfpcontest2019.github.io/solution_checker/')

    def get_taskinput_elm(self):
        return self.driver.find_element_by_id('submit_task')

    def get_solution_input_elm(self):
        return self.driver.find_element_by_id('submit_solution')

    def check(self, task_path, sol_path) -> str:
        self.get_taskinput_elm().send_keys(task_path)
        self.get_solution_input_elm().send_keys(sol_path)
        time.sleep(1)
        self.driver.find_element_by_id('execute_solution').click()
        time.sleep(3)

        return self.driver.find_element_by_id('output').text

def main():
    import sys
    exepath = sys.argv[1]
    task = sys.argv[2]
    sol = sys.argv[3]
    checker = Checker(exepath)
    output = checker.check(task, sol)
    print(output)

if __name__ == '__main__':
    main()

