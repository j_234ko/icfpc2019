from pathlib import Path
from datetime import datetime, timedelta
from flask import Flask, render_template, jsonify, request

app = Flask('cirimenjao-score-manager',
            static_url_path = '',
            static_folder = 'static')


@app.before_request
def _before_request():
    from db import engine
    from model import Solution
    Solution.metadata.create_all(bind=engine)


@app.after_request
def _after_request(response):
    from db import session
    session.remove()
    return response


@app.route('/', methods=['GET'])
def index():
    from model import Solution
    from db import session

    query = session.query(Solution)
    data = {}
    keys = []
    for s in query:
        if s.prob_id not in data:
            data[s.prob_id] = {}

        jst = datetime.strptime(s.uniq_key, '%m%d%H%M') + timedelta(hours=9)
        jst = jst.strftime('%m-%d %H:%M')
        data[s.prob_id][jst] = s.export_dict()
        if jst not in [k['key'] for k in keys]:
            keys.append({'key': jst, 'uniq_key': s.uniq_key})

    keys.sort(key = lambda x: x['key'])
    result = [keys]

    min_dic = {}
    for prob_id in range(1, 301, 1):
        min_dic[prob_id] = min([d['score'] for d in data[prob_id].values()
                                if d['score'] != 0])

    for prob_id in range(1, 301, 1):
        for d in data[prob_id].values():
            if d['score'] == min_dic[prob_id]:
                d['min'] = 1
            else:
                d['min'] = 0

    for prob_id in range(1, 301, 1):
        if prob_id not in data:
            continue
        for k in keys:
            if k['key'] not in data[prob_id]:
                data[prob_id][k['key']] = {'score': 0}
        result.append([data[prob_id][k['key']] for k in keys])

    return render_template('index.html', result=result)


@app.route('/zip/<uniq_key>/', methods=['POST'])
def make_zip(uniq_key):
    from service import make_zip
    zip_path = make_zip(uniq_key)
    return jsonify({'result': 'success'})


@app.route('/map/<uniq_key>/<submit_time>/', methods=['POST'])
def map_scores(uniq_key, submit_time):
    from service import map_scores
    map_scores(uniq_key, submit_time)
    return jsonify({'uniq_key': uniq_key, 'submit_time': submit_time})


if __name__ == '__main__':
    app.run(host='0.0.0.0')
