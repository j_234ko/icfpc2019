import re
import requests
import botocore
import json
from boto3.session import Session
from zipfile import ZipFile
from datetime import datetime, timedelta
from pathlib import Path
from model import Solution
from db import session


def make_dirs(path: Path):
    try:
        path.mkdir()
    except FileNotFoundError:
        make_dirs(path.parent)
        path.mkdir()
    except FileExistsError:
        pass


def get_solution(uniq_key: str, prob_id: int, target_dir: Path):
    profile = 'cirimenjao'
    boto_session = Session(profile_name=profile)

    s3 = boto_session.resource('s3')
    bucket = s3.Bucket('cirimenjao')

    sol_name = f'prob-{str(prob_id).zfill(3)}.sol'
    target_path = target_dir / sol_name
    try:
        bucket.download_file(Key=f'{uniq_key}/{target_path.name}',
                             Filename=str(target_path))
        print(f'Download: {uniq_key}/{prob_id}')
    except botocore.exceptions.ClientError:
        print(f'Not Found: {uniq_key}/{prob_id}')
        return

    zip_path = target_dir / 'solutions.zip'
    with ZipFile(str(zip_path), 'a') as z:
        z.write(str(target_path), arcname=target_path.name)
        print(f'Zip: {uniq_key}/{prob_id}')


def make_zip(uniq_key: str) -> Path:
    target_dir = Path.home() / 'icfpc2019' / 'sol' / uniq_key
    make_dirs(target_dir)

    for prob_id in range(1, 301, 1):
        get_solution(uniq_key, prob_id, target_dir)


def map_scores(uniq_key: str, submit_time: str):
    url = ('https://monadic-lab.org/grades/4e6e9a87b9e0fab6accf7f59/'
           f'{submit_time}/score.csv')
    r = requests.get(url)
    for line in r.text.splitlines():
        prob_id, score, status = line.split(', ')
        solution = Solution.import_dict({
            'prob_id': prob_id,
            'uniq_key': uniq_key,
            'submit_time': submit_time,
            'score': score,
            'status': status,
        })
        session.add(solution)

    session.commit()


def choose_best():
    target_dir = Path.home() / 'icfpc2019' / 'best'
    make_dirs(target_dir)
    best_dic = {}
    for prob_id in range(1, 301, 1):
        best = session.query(Solution).filter(
            Solution.status=='Ok').filter(Solution.prob_id==prob_id).order_by(
                Solution.score).first()
        if best:
            get_solution(best.uniq_key, prob_id, target_dir)
        else:
            print(f'Not Found: {prob_id}')
            continue

        jst = datetime.strptime(best.uniq_key, '%m%d%H%M') + timedelta(hours=9)
        jst = jst.strftime('%m-%d %H:%M')
        if jst not in best_dic:
            best_dic[jst] = 1
        else:
            best_dic[jst] += 1

    print(json.dumps(best_dic, indent=4))


if __name__ == '__main__':
    choose_best()
