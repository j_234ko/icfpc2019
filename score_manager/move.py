from pathlib import Path

p_dir = Path.home() / 'icfpc2019' / 'score_manager'
print(p_dir)
for p in p_dir.glob('**/*.png'):
    prob_id = int(p.name.replace('prob-', '').replace('.desc.png', ''))
    print(prob_id)
    p.rename(f'{prob_id}.png')
