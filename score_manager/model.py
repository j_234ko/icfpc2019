# -*- coding: utf-8 -*-
import json
from sqlalchemy import Column, Integer, String, Float
from db import Base


class Entity:

    @classmethod
    def key_column(cls):
        return

    @classmethod
    def get_by_key(cls, data):
        from db import session
        key_column = cls.key_column()
        if key_column is None:
            return
        if not isinstance(key_column, (list, tuple)):
            key_column = [key_column, ]
        if any(c not in data for c in key_column):
            return
        exp = [getattr(cls, c) == data[c] for c in key_column]
        entity = session.query(cls).filter(*exp).first()
        return entity

    @classmethod
    def import_dict(cls, data):
        from datetime import datetime
        now = datetime.now()

        entity = cls.get_by_key(data)
        if entity is None:
            entity = cls()
            if hasattr(entity, 'created_at'):
                entity.created_at = now

        if hasattr(entity, 'modified_at'):
            entity.modified_at = now

        for c in cls.__table__.columns:
            if c.key in ('created_at', 'modified_at'):
                continue
            if c.key in data:
                setattr(entity, c.key, data[c.key])

        return entity

    @classmethod
    def export_exclude(self):
        return []

    def export_dict(self):
        data = {}
        for c in self.__table__.columns:
            if c.key == '%s_id' % self.__table__.name:
                continue
            if c.key in self.export_exclude():
                continue
            data[c.key] = getattr(self, c.key)
        data = self.on_prepare_export(data)
        return data

    def on_prepare_export(self, data):
        return data


class Solution(Base, Entity):
    __tablename__ = 'solution'

    sol_id = Column(Integer, primary_key=True)
    uniq_key = Column(String, nullable=False)
    prob_id = Column(Integer, nullable=False)
    score = Column(Integer, nullable=True)
    submit_time = Column(String, nullable=True)
    status = Column(String, nullable=True)

    @classmethod
    def key_column(cls):
        return ['prob_id', 'uniq_key']
