from sqlalchemy import create_engine, MetaData, Table
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('sqlite:///db.sqlite3', echo=False)
metadata = MetaData(bind=engine)
Base = declarative_base()
session = scoped_session(
    sessionmaker(autoflush=True, autocommit=False, bind=engine))
