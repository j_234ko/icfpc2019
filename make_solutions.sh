#!/bin/bash

while getopts :c OPT
do
    case $OPT in
        c)  CLEAR=1
            ;;
    esac
done

shift $((OPTIND - 1))

if [ "$CLEAR" ]; then
    if [ -e solutions.zip ]; then
        rm solutions.zip
    fi
    for s in sol/*.sol; do
        rm $s
    done
fi

aws s3 cp s3://cirimenjao/$1/ sol/ --recursive --profile cirimenjao
cd sol
zip -r solutions.zip *.sol
mv solutions.zip ..
cd ..
